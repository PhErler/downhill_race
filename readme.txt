Downhill Race

Downhill Race is a fluffy racing game by Carina Pratsch and Philipp Erler. 
Up to 4 players in split-screen steer sheep through a rural area into the goal. 
This game is completely free and open source.
It is run by a self-made game engine using OpenGl 3.3 with shadowmapping and volume rendering.


Controls

Player 1: WASD
Player 2: Arrow Keys
Player 3: IJKL
Player 4: Num-8456

You can also use (XInput) game controllers. 
Accelerate and break with the triggers. Steer with the left stick.


How to start

Double-click the start.bat

You can modify the start.bat to set the parameters:
width height updateRate fullscreen numPlayers[1-4]
