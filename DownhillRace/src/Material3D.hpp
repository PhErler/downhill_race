#pragma once

#include "Texture3D.hpp"
#include "Shader.hpp"
#include "ResourceManager.hpp"
#include "IMaterial.hpp"
#include "RenderTexture.hpp"
#include "FrameBuffer.hpp"

#include <string>

#include <GL/glew.h>

namespace SheepEngine
{
	namespace RenderPasses
	{
		enum Enum
		{
			EYE = 0,
			LIGHT = 1,
			COUNT = 2
		};
	}

	class Material3D : public IMaterial
	{
	public:
		Material3D(
			std::string modelPath = std::string(),
			std::string vertexShaderPath = std::string(),
			std::string fragmentShaderPath = std::string(),
			std::string geometryShaderPath = std::string());
		virtual ~Material3D();

		virtual void render(
			int playerNumber,
			const glm::mat4 &modelMatrix, 
			const glm::mat4 &viewMatrix, 
			const glm::mat4 &projectionMatrix);

		// z-buffer pass for shadow mapping
		virtual void renderZ(
			const glm::mat4 &modelMatrix,
			const glm::mat4 &lightViewMatrix,
			const glm::mat4 &lightProjectionMatrix);

		std::shared_ptr<ModelData> getModelData() { return _modelData; }
		void setTexture3D(std::shared_ptr<Texture3D> texture3D) { _texture = texture3D; }

		static bool enableBlending;

		virtual void switchSamplingMethod(int newMinSamplingMethod, int newMaxSamplingMethod);

	protected:
		std::shared_ptr<Shader> _eyeShader;
		std::shared_ptr<Shader> _lightShader;
		std::shared_ptr<Shader> _passthroughShader;
		std::shared_ptr<ModelData> _modelData;
		std::shared_ptr<Texture3D> _texture;

		RenderTexture* _lightTexture = nullptr;
		std::shared_ptr<FrameBuffer> _lightBuffer;
		RenderTexture* _eyeTexture;
		FrameBuffer* _eyeBuffer;

		std::vector<glm::vec3> _quadVertices;
		VertexBuffer* _quadVertexBuffer;

		GLuint _textureID[RenderPasses::COUNT];
		GLuint _pvMatrixID[RenderPasses::COUNT];
		GLuint _invModelMatrixID[RenderPasses::COUNT];
		GLuint _sizeID[RenderPasses::COUNT];
		GLuint _divRefByCurSamplingRateID[RenderPasses::COUNT];
		GLuint _sliceModelID[RenderPasses::COUNT];
		GLuint _lightTextureID[RenderPasses::COUNT];
		GLuint _lightPVID[RenderPasses::COUNT];

		GLuint _eyeTextureID; // only passthrough

		virtual void eyePass(
			const glm::mat4 &sliceModelMatrix,
			const glm::mat4 &modelMatrix,
			const glm::mat4 &viewMatrix,
			const glm::mat4 &projectionMatrix,
			float sliceZ,
			glm::mat4 &halfAngleRot,
			float dirFactor,
			float currentSamplingRate,
			glm::mat4 &invLightMVP,
			glm::mat4 &lightV,
			glm::mat4 &lightP);

		virtual void lightPass(
			const glm::mat4 &sliceModelMatrix,
			const glm::mat4 &modelMatrix,
			const glm::mat4 &viewMatrix,
			const glm::mat4 &projectionMatrix,
			float sliceZ,
			glm::mat4 &halfAngleRot,
			float dirFactor,
			float currentSamplingRate,
			glm::mat4 &invLightMVP,
			glm::mat4 &lightV,
			glm::mat4 &lightP);

		virtual void renderEyeBufferToRenderBuffer(int playerNumber);

	private:
		glm::mat4 removeRotation(const glm::mat4 &m);
	};
}

