#include "Tree.hpp"

#include "SheepEngineMain.hpp"
#include "Material.hpp"

#include <algorithm>    // std::max
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>

using namespace std;
using namespace glm;
using namespace SheepEngine;

Tree::Tree(int id, glm::mat4 initialTransform, glm::vec3 scale)
: PhysicsGameObject(scale)
{

	string modelpath;

	if (id == 1) modelpath = "resources\\models\\tree.obj";
	if (id == 2) modelpath = "resources\\models\\tree_stump.obj";
	if (id == 3) modelpath = "resources\\models\\tree_trunk.obj";

	Material *newMaterial = new Material(modelpath,
		"resources\\textures\\stuff_texture.bmp",
		"resources\\shaders\\ShadowMapping.vertexshader",
		"resources\\shaders\\ShadowMapping.fragmentshader");
	_material = newMaterial;

	btTransform tempTransform;
	tempTransform.setFromOpenGLMatrix(glm::value_ptr(initialTransform));
	_rigidBody->setWorldTransform(tempTransform);

	_rigidBody->setLinearFactor(btVector3(0, 0, 0)); // should never move
	_rigidBody->setAngularFactor(btVector3(0, 0, 0)); // should never rotate

	// Triangle Collision Shape
	btTriangleMesh *mTriMesh = newMaterial->getTriangleMesh();

	btCollisionShape *mTriMeshShape = new btBvhTriangleMeshShape(mTriMesh, true);
	_rigidBody->setCollisionShape(mTriMeshShape);

	_rigidBody->setMassProps(1, btVector3(1, 1, 1));
}

Tree::~Tree()
{
}
