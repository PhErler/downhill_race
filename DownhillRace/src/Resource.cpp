#include "Resource.hpp"

#include "Shader.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <iostream>

using namespace std;
using namespace SheepEngine;

IResource::IResource()
: _resourceID(0)
{
}

IResource::~IResource()
{
}

GLuint IResource::getResourceID()
{
	return _resourceID;
}



GLuint ITexture::s_currentActiveTexture = 0;

ITexture::ITexture()
: _resourceID(0)
{
}

ITexture::~ITexture()
{
}

GLuint ITexture::getResourceID()
{
	return _resourceID;
}

void ITexture::activateTextureUnit(GLuint unitID)
{
	if (s_currentActiveTexture != unitID)
	{
		glActiveTexture(GL_TEXTURE0 + unitID);
		s_currentActiveTexture = unitID;
	}
}

// basing on the opengl-tutorial from
// http://www.opengl-tutorial.org/
void ITexture::loadBMP(string path, vector<char> &data_out, int &width_out, int &height_out)
{

	printf("Reading image %s\n", path.c_str());

	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;
	// Actual RGB data
	//unsigned char * data;

	// Open the file
	FILE * file = fopen(path.c_str(), "rb");
	if (!file)							    { printf("%s could not be opened. Are you in the right directory?\n", path.c_str()); getchar(); return; }

	// Read the header, i.e. the 54 first bytes

	// If less than 54 bytes are read, problem
	if (fread(header, 1, 54, file) != 54){
		printf("Not a correct BMP file\n");
		return;
	}
	// A BMP files always begins with "BM"
	if (header[0] != 'B' || header[1] != 'M'){
		printf("Not a correct BMP file\n");
		return;
	}
	// Make sure this is a 24bpp file
	if (*(int*)&(header[0x1E]) != 0)         { printf("Not a correct BMP file\n");    return; }
	if (*(int*)&(header[0x1C]) != 24)         { printf("Not a correct BMP file\n");    return; }

	// Read the information about the image
	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	width = *(int*)&(header[0x12]);
	height = *(int*)&(header[0x16]);

	// Some BMP files are misformatted, guess missing information
	if (imageSize == 0)    imageSize = width*height * 3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos == 0)      dataPos = 54; // The BMP header is done that way

	width_out = width;
	height_out = height;

	// Create a buffer
	//data = new unsigned char[imageSize];
	data_out = vector<char>(imageSize);

	// Read the actual data from the file into the buffer
	fread(&data_out[0], 1, imageSize, file);

	// Everything is in memory now, the file wan be closed
	fclose(file);

	// OpenGL has now copied the data. Free our own version
	//delete[] data;
}


VertexArray::VertexArray()
{
	glGenVertexArrays(1, &_resourceID);
	glBindVertexArray(_resourceID);
}

VertexArray::~VertexArray()
{
	glDeleteVertexArrays(1, &_resourceID);
}

void VertexArray::activate()
{
}

void VertexArray::deactivate()
{
}



VertexBuffer::VertexBuffer(const std::vector<glm::vec3> &vertices, int drawType)
{
	_vertices = vertices;
	glGenBuffers(1, &_resourceID);
	update(vertices, drawType);
}

VertexBuffer::~VertexBuffer()
{
	glDeleteBuffers(1, &_resourceID);
}

void VertexBuffer::update(const std::vector<glm::vec3> &vertices, int drawType)
{
	if (!vertices.empty())
	{
		glBindBuffer(GL_ARRAY_BUFFER, _resourceID);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3),
			&vertices[0], drawType);
	}
}

void VertexBuffer::activate()
{
	// 1st attribute buffer : _vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, _resourceID);
	glVertexAttribPointer(
		0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);
}

void VertexBuffer::deactivate()
{
	glDisableVertexAttribArray(0);
}



UVBuffer::UVBuffer(const std::vector<glm::vec2> &uvs)
{
	_uvs = uvs;
	glGenBuffers(1, &_resourceID);
	glBindBuffer(GL_ARRAY_BUFFER, _resourceID);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), 
		&uvs[0], GL_STATIC_DRAW);
}

UVBuffer::~UVBuffer()
{
	glDeleteBuffers(1, &_resourceID);
}

void UVBuffer::activate()
{
	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, _resourceID);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		2,                                // size : U+V => 2
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);
}

void UVBuffer::deactivate()
{
	glDisableVertexAttribArray(1);
}



NormalBuffer::NormalBuffer(const std::vector<glm::vec3> &normals)
{
	_normals = normals;
	glGenBuffers(1, &_resourceID);
	glBindBuffer(GL_ARRAY_BUFFER, _resourceID);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3),
		&normals[0], GL_STATIC_DRAW);
}

NormalBuffer::~NormalBuffer()
{
	glDeleteBuffers(1, &_resourceID);
}

void NormalBuffer::activate()
{
	// 3rd attribute buffer : normals
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, _resourceID);
	glVertexAttribPointer(
		2,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);
}

void NormalBuffer::deactivate()
{
	glDisableVertexAttribArray(2);
}