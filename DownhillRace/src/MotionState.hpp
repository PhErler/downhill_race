#pragma once

#include <btBulletDynamicsCommon.h>

namespace SheepEngine
{
	class PhysicsGameObject;

	class MotionState : public btMotionState
	{
	public:
		MotionState(PhysicsGameObject *gameObject);

		virtual void setWorldTransform(const btTransform &worldTrans);
		virtual void getWorldTransform(btTransform& worldTrans) const;

	protected:
		PhysicsGameObject *_gameObject;
	};
}

