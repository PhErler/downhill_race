#include "DRGameManager.hpp"
#include "SheepEngineMain.hpp"

using namespace SheepEngine;

int main(int argc, char* argv[])
{
	DRGameManager gameManager;
	SheepEngineMain::setGameManager(&gameManager);

	int error = SheepEngineMain::init(argc, argv);
	if (error == EXIT_SUCCESS)
	{
		error = SheepEngineMain::run();
	}
	error = SheepEngineMain::deinit();

	return error;
}