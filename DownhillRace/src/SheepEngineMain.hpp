#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <memory>

#include <btBulletDynamicsCommon.h>
#include <BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "GameObject.hpp"
#include "RenderTexture.hpp"
#include "FrameBuffer.hpp"
#include "IGameManager.hpp"
#include "Shader.hpp"
#include "Resource.hpp"

namespace SheepEngine
{
	struct WindowParams
	{
		WindowParams(GLuint xRes,
		GLuint yRes,
		GLuint refreshRate,
		bool fullScreen,
		int numPlayers)
		: _xRes(xRes)
		, _yRes(yRes)
		, _refreshRate(refreshRate)
		, _fullScreen(fullScreen)
		, _numPlayers(numPlayers)
		{};

		GLuint _xRes;
		GLuint _yRes;
		GLuint _refreshRate;
		bool _fullScreen;
		int _numPlayers;
	};

	// to avoid opengl state changes
	struct Rect
	{
		Rect(GLuint left,
		GLuint top,
		GLuint width,
		GLuint height)
		: _left(left)
		, _top(top)
		, _width(width)
		, _height(height)
		{};

		GLuint _left;
		GLuint _top;
		GLuint _width;
		GLuint _height;
	};

	class SheepEngineMain
	{
	public:
		static int init(int argc, char* argv[]);
		static int run();
		static int deinit();

		static GLFWwindow* getWindow();
		static const WindowParams& getWindowParams();

		static btDiscreteDynamicsWorld* getPhysicsWorld() { return s_physicsWorld; }

		static void addGameObject(GameObject* gameObject){ s_gameObjects.push_back(gameObject); }
		static void setGameManager(IGameManager* gameManager){ s_gameManager = gameManager; }
		static IGameManager* getGameManager(){ return s_gameManager; }

		static FrameBuffer* getShadowFrameBuffer(){ return s_shadowFrameBuffer; };
		static std::shared_ptr<Shader> getdepthProgramID(){ return s_depthProgram; };
		static GLuint getLightMVPID(){ return s_lightMVPID; };
		static void setShadowMappingSampleDistance(float shadowMappingSampleDistance){ s_shadowMappingSampleDistance = shadowMappingSampleDistance; };
		static float getShadowMappingSampleDistance(){ return s_shadowMappingSampleDistance; };
		static RenderTexture* getShadowTexture(){ return s_shadowTexture; };
		static glm::mat4 getLightViewMatrix(){ return s_lightViewMatrix; };
		static void setLightProjectionMatrix(const glm::mat4 &newMatrix){ s_lightProjectionMatrix = newMatrix; };
		static glm::mat4 getLightProjectionMatrix(){ return s_lightProjectionMatrix; };

		static glm::vec3 getLightDir() { return s_LightDir; }
		static glm::vec3 getLightColor() { return s_LightColor; }
		static glm::vec3 getAmbientColor() { return s_AmbientColor; }
		static glm::vec3 getSpecularColor() { return s_SpecularColor; }

		static void setLightDir(glm::vec3 lightDir);
		static void setLightColor(glm::vec3 lightColor);
		static void setAmbientColor(glm::vec3 ambientColor);
		static void setSpecularColor(glm::vec3 specularColor);

		// viewfrustum culling
		static int s_verticesRenderedThisFrame;
		static bool s_cullingEnabled;
		static bool isCullingEnabled(){ return s_cullingEnabled; }
		static void addRenderedVertices(int count);
		static int getRenderedVertices();
		static void toggleCulling();

		// wireframe mode
		static bool wireFrameMode;
		static void changeWireFrameMode();

		static void switchSamplingMethod(int newMinSamplingMethod, int newMaxSamplingMethod);
		static void bulletSubStepCallback(btDynamicsWorld *world, btScalar timeStep);

		// frames per second
		static double getFPS();

		static void setViewPort(Rect viewPort);

	private:
		static WindowParams parseCommandLineParams(int argc, char* argv[]);
		static void initSound();
		static void deinitSound();
		static void initShadowMapping();
		static void shadingPass();
		static void zBufferPass();

		static void audioCallback(void *userdata, unsigned char *stream, int len);

		static bool s_initialized;
		static int s_errorCode;
		static WindowParams s_windowParams;
		static GLFWwindow* s_window;
		static IGameManager* s_gameManager;

		// light info
		static glm::vec3 s_LightDir;
		static glm::vec3 s_LightColor;
		static glm::vec3 s_AmbientColor;
		static glm::vec3 s_SpecularColor;

		// shadow mapping
		static FrameBuffer* s_shadowFrameBuffer;
		static std::shared_ptr<Shader> s_depthProgram;
		static GLuint s_lightMVPID;
		static RenderTexture* s_shadowTexture;
		static glm::mat4 s_lightViewMatrix;
		static glm::mat4 s_lightProjectionMatrix;
		static float s_shadowMappingSampleDistance;

		// bullet stuff
		static btBroadphaseInterface* s_broadphase;
		static btDefaultCollisionConfiguration* s_collisionConfiguration;
		static btCollisionDispatcher* s_dispatcher;
		static btSequentialImpulseConstraintSolver* s_solver;
		static btDiscreteDynamicsWorld* s_physicsWorld;

		// a scene forest structure
		static std::vector<GameObject*> s_gameObjects;

		static double s_fps;
		static Rect s_currViewport;

		static unsigned char *s_audioPos; // global pointer to the audio buffer to be played
		static unsigned int s_audioLen; // remaining length of the sample we have to play
		static unsigned char *s_wavBuffer; // buffer containing our audio file
	};
}

