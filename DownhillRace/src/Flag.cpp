#include "Flag.hpp"

#include "SheepEngineMain.hpp"
#include "Material.hpp"

#include <algorithm>    // std::max
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>

using namespace std;
using namespace glm;
using namespace SheepEngine;

Flag::Flag(glm::mat4 initialTransform, glm::vec3 scale)
: PhysicsGameObject(scale)
{
	Material *newMaterial = new Material("resources\\models\\flag.obj",
		"resources\\textures\\flag_texture.bmp",
		"resources\\shaders\\ShadowMapping.vertexshader",
		"resources\\shaders\\ShadowMapping.fragmentshader");
	newMaterial->setTextureTiling(vec2(2, 1));
	_material = newMaterial;

	// flag size
	vec3 max = newMaterial->getModelData()->_maxBB;
	vec3 min = newMaterial->getModelData()->_minBB;
	_size = max - min;

	btTransform tempTransform;
	tempTransform.setFromOpenGLMatrix(glm::value_ptr(initialTransform));
	_rigidBody->setWorldTransform(tempTransform);

	_rigidBody->setLinearFactor(btVector3(0, 0, 0)); // should never move
	_rigidBody->setAngularFactor(btVector3(0, 0, 0)); // should never rotate

	// Triangle Collision Shape
	/*btTriangleMesh *mTriMesh = newMaterial->getTriangleMesh();
	btCollisionShape *mTriMeshShape = new btBvhTriangleMeshShape(mTriMesh, true);
	_rigidBody->setCollisionShape(mTriMeshShape);*/
	_rigidBody->setCollisionShape(new btBoxShape(btVector3(_size.x * 0.7f, _size.y * 0.5f, _size.z * 0.5f)));
	_rigidBody->setMassProps(1, btVector3(1, 1, 1));
	_rigidBody->setCollisionFlags(
		_rigidBody->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);
}

Flag::~Flag()
{
}
