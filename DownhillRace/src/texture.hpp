#pragma once

#include "Resource.hpp"

#include <string>
#include <vector>

namespace SheepEngine
{
	class Texture : public ITexture
	{
	public:
		Texture(std::string path);
		virtual ~Texture();

		virtual void activate(unsigned int textureSampler);
		virtual void deactivate(unsigned int textureSampler);

		void switchSamplingMethod(int newMinSamplingMethod, int newMaxSamplingMethod);

		void loadImage(std::string path, int newMinSamplingMethod = GL_LINEAR_MIPMAP_LINEAR, int newMaxSamplingMethod = GL_LINEAR);
		void unloadImage();

	private:
		// Load a .BMP file using our custom loader
		GLuint loadBMP_custom(std::string imagepath, int newMinSamplingMethod, int newMaxSamplingMethod);

		// Load a .DDS file using GLFW's own loader
		GLuint loadDDS(const char * imagepath, int newMinSamplingMethod, int newMaxSamplingMethod);

		std::string _path;
	};
}
