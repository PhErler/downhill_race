#pragma once

#include "IMaterial.hpp"

#include <vector>
#include <string>

#include <GL/glew.h>

#include <glm/glm.hpp>

#include <btBulletDynamicsCommon.h>

namespace SheepEngine
{
	class GameObject
	{
	public:
		GameObject();
		virtual ~GameObject();

		virtual void update(double dt);
		virtual void render(int playerCam);
		virtual void renderZ();

		bool isVisible(){ return _isVisible; }
		void setVisibility(bool visible){ _isVisible = visible; }

		void setMaterial(IMaterial* material);

		void setParent(GameObject* parent);
		virtual const GameObject* getParent();
		virtual void addChild(GameObject * child);

		void translate(float x, float y, float z);
		void rotate(float x, float y, float z, float angle);
		void scale(float x, float y, float z);

		void animateRotate(float x, float y, float z, float angle);
		void animateTranslate(float x, float y, float z);
		bool isAnimated();
		glm::vec4 getRotate();

		virtual void setModelMatrix(const glm::mat4 &newMatrix);
		virtual const glm::mat4& getModelMatrix() const; // without parent's modelmatrix
		virtual const glm::mat4 getWorldspaceMatrix() const; // including parent's modelmatrix

		virtual glm::vec3 getPosition();
		virtual void setPosition(glm::vec3 position);

		void setAnimatetReverse(float angle);

		void switchSamplingMethod(int newMinSamplingMethod, int newMaxSamplingMethod);

	protected:
		IMaterial* _material = nullptr;

		GameObject* _parent = nullptr;
		bool _animationRot = false;
		bool _animationTrans = false;
		float _animateAngle[2];
		bool _animateDirection[2];
		std::vector<GameObject*> _children;
		bool _isVisible = true;

		glm::mat4 _modelMatrix;
		glm::vec4 _rotVec;
		glm::mat4 _transMatrix;
	};
}

