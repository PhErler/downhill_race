#pragma once

#include "Resource.hpp"

namespace SheepEngine
{
	class RenderTexture;

	class FrameBuffer : public IResource
	{
	public:
		FrameBuffer(RenderTexture* colorTexture, RenderTexture* depthTexture);
		virtual ~FrameBuffer();

		void updateRenderTexture(RenderTexture* colorTexture, RenderTexture* depthTexture);

		virtual void activate();
		virtual void deactivate();

		virtual void clear(bool clearColor, bool clearDepth, 
			float r = 0, float g = 0, float b = 0, float a = 0);

	protected:
		GLuint _width = 0;
		GLuint _height = 0;
	};
}
