#include "Camera.hpp"

#include "SheepEngineMain.hpp"
#include "CameraManager.hpp"

#include "glm\gtc\matrix_transform.hpp"

using namespace std;
using namespace glm;
using namespace SheepEngine;

namespace
{
	// Initial Field of View
	const float InitialFoV = 45.0f;

	const float Speed = 3.0f; // 3 units / second
	const float MouseSpeed = 0.005f;

	const float ZNearPlane = 0.1f;
	const float ZFarPlane = 150.0f;
}

Camera::Camera()
: _position(0, 2, 25)	// Initial position : on +Z
{
	GLFWwindow* window = SheepEngineMain::getWindow();

	_cameraIndex = CameraManager::addCamera(this);

	int xRes = 0;
	int yRes = 0;
	CameraManager::calcRenderbufferSize(xRes, yRes);
	float windowAspect = static_cast<float>(xRes) / static_cast<float>(yRes);

	mat4 projectionMatrix = glm::perspective(InitialFoV, windowAspect, ZNearPlane, ZFarPlane);
	CameraManager::setProjectionMatrix(_cameraIndex, projectionMatrix);
}

// basing on the opengl-tutorial from
// http://www.opengl-tutorial.org/
void Camera::update(double dt)
{
	GLFWwindow* window = SheepEngineMain::getWindow();
	GLuint xRes = SheepEngineMain::getWindowParams()._xRes;
	GLuint yRes = SheepEngineMain::getWindowParams()._yRes;

	float deltaTime = float(dt);

	// Get mouse position
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	// Reset mouse position for next frame
	glfwSetCursorPos(window, xRes / 2, yRes / 2);

	// Compute new orientation
	_horizontalAngle += MouseSpeed * float(xRes / 2 - xpos);
	_verticalAngle += MouseSpeed * float(yRes / 2 - ypos);

	// Direction : Spherical coordinates to Cartesian coordinates conversion
	glm::vec3 direction(
		cos(_verticalAngle) * sin(_horizontalAngle),
		sin(_verticalAngle),
		cos(_verticalAngle) * cos(_horizontalAngle)
		);

	// Right vector
	glm::vec3 right = glm::vec3(
		sin(_horizontalAngle - 3.14f / 2.0f),
		0,
		cos(_horizontalAngle - 3.14f / 2.0f)
		);

	// Up vector
	glm::vec3 up = glm::cross(right, direction);

	// Move forward
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS){
		_position += direction * deltaTime * Speed;
	}
	// Move backward
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS){
		_position -= direction * deltaTime * Speed;
	}
	// Strafe right
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS){
		_position += right * deltaTime * Speed;
	}
	// Strafe left
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS){
		_position -= right * deltaTime * Speed;
	}

	// Camera matrix
	mat4 viewMatrix = glm::lookAt(
		_position,				// Camera is here
		_position + direction,	// and looks here : at the same position, plus "direction"
		up						// Head is up (set to 0,-1,0 to look upside-down)
		);

	CameraManager::setViewMatrix(_cameraIndex, viewMatrix);

	_modelMatrix[3][0] = _position.x;
	_modelMatrix[3][1] = _position.y;
	_modelMatrix[3][2] = _position.z;
	_modelMatrix[3][3] = 1;
}

void Camera::render()
{
	// do nothing, prevent rendering of base class GameObject
}

Camera::~Camera()
{
	CameraManager::removeCamera(_cameraIndex);
}