#pragma once

#include "Resource.hpp"

namespace SheepEngine
{
	typedef std::vector<std::vector<std::vector<glm::vec4>>> tex3D;

	class Texture3D : public ITexture
	{
	public:
		Texture3D(int playerNumber);
		virtual ~Texture3D();

		virtual void activate(unsigned int textureSampler);
		virtual void deactivate(unsigned int textureSampler);

		void switchSamplingMethod(int newMinSamplingMethod, int newMaxSamplingMethod);

	private:
		void createWindowTestTexture(tex3D &tex);
		void createBricksTestTexture(tex3D &tex);
		void createSlicesTestTexture(tex3D &tex);
		void createShadowTestTexture(tex3D &tex);

		void createWoolTexture(tex3D &tex, int playerNumber);
	};
}
