#pragma once

#include "IGameManager.hpp"
#include "GameObject.hpp"
#include "texture.hpp"
#include "Shader.hpp"

#include <vector>
#include <memory>

#include <glm/glm.hpp>

class btDynamicsWorld;

namespace SheepEngine
{
	class Sheep;
	class Flag;
	class Material;

	class DRGameManager : public IGameManager
	{
	public:
		DRGameManager();
		virtual ~DRGameManager();

		virtual void init();
		virtual void deinit();

		virtual void update(double dt);
		virtual void render();

		GameObject* getPartyHat(){ return _partyHat; }
		Material* getDuckMaterial(){ return _duckMaterial; }

		virtual void bulletSubStep(btDynamicsWorld *world, float timeStep);

		static bool frameTime;

	private:
		std::vector<float> _finishTime;

		std::vector<Sheep*> _sheeps;
		std::shared_ptr<Shader> _passthroughShader;
		unsigned int _renderTextureID;
		VertexBuffer* _quadVertexBuffer;
		std::vector<int> _places;
		std::vector<std::shared_ptr<Texture>> _placeTexture;
		int _finishedPlayers;
		GameObject* _partyHat = nullptr;
		Material* _duckMaterial = nullptr;

		Flag* _start;
		Flag* _finish;

		bool isCollisionObjectFinish(const btCollisionObject* collObj);
		int DRGameManager::getSheepIndex(const btCollisionObject* collObj);

		void updateFinishStates(btDynamicsWorld *world);
	};
}

