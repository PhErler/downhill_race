#include "Material3D.hpp"

#include "SheepEngineMain.hpp"
#include "CameraManager.hpp"
#include "Texture.hpp"
#include "VboIndexer.hpp"
#include "Camera.hpp"

#include "glm\gtx\transform.hpp"
#include "glm\gtc\constants.hpp"

using namespace std;
using namespace glm;
using namespace SheepEngine;

bool Material3D::enableBlending = true;

namespace
{
	const float STANDARD_SAMPLING_RATE = 50;
	const float REFERENCE_SAMPLING_RATE = 50;
	const float VOLUME_SIZE_FACTOR = 2;
	const unsigned int LIGHT_TEXTURE_SIZE = 256;

	const GLfloat QUAD_VERTICES[] = {
		-1.0f, -1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		 1.0f,  1.0f, 0.0f,
	};
}

Material3D::Material3D(
	std::string modelPath,
	std::string vertexShaderPath,
	std::string fragmentShaderPath,
	std::string geometryShaderPath)
{
	_eyeShader = ResourceManager::getShader(vertexShaderPath, fragmentShaderPath, geometryShaderPath);
	_lightShader = ResourceManager::getShader(
		"resources\\shaders\\VolumeLightRendering.vertexshader",
		"resources\\shaders\\VolumeLightRendering.fragmentshader");
	_passthroughShader = ResourceManager::getShader(
		"resources\\shaders\\Passthrough.vertexshader",
		"resources\\shaders\\Passthrough.fragmentshader");

	// plane made of 2 triangles
	_quadVertices.push_back(vec3(QUAD_VERTICES[0], QUAD_VERTICES[1], QUAD_VERTICES[2]));
	_quadVertices.push_back(vec3(QUAD_VERTICES[3], QUAD_VERTICES[4], QUAD_VERTICES[5]));
	_quadVertices.push_back(vec3(QUAD_VERTICES[6], QUAD_VERTICES[7], QUAD_VERTICES[8]));
	_quadVertices.push_back(vec3(QUAD_VERTICES[9], QUAD_VERTICES[10], QUAD_VERTICES[11]));
	_quadVertices.push_back(vec3(QUAD_VERTICES[12], QUAD_VERTICES[13], QUAD_VERTICES[14]));
	_quadVertices.push_back(vec3(QUAD_VERTICES[15], QUAD_VERTICES[16], QUAD_VERTICES[17]));
	_quadVertexBuffer = new VertexBuffer(_quadVertices);

	// Read our .obj file
	if (modelPath.length() != 0)
	{
		_modelData = ResourceManager::getModelData(modelPath);
	}

	_lightTexture = new RenderTexture(LIGHT_TEXTURE_SIZE, LIGHT_TEXTURE_SIZE, RenderTextureType::RGBA);
	_lightBuffer = std::make_shared<FrameBuffer>(_lightTexture, nullptr);

	int xRes = 0;
	int yRes = 0;
	CameraManager::calcRenderbufferSize(xRes, yRes);
	_eyeTexture = new RenderTexture(xRes, yRes, RenderTextureType::RGBA);
	_eyeBuffer = new FrameBuffer(_eyeTexture, nullptr);

	GLuint programID;
	for (int i = 0; i < RenderPasses::COUNT; ++i)
	{
		switch (i)
		{
		case RenderPasses::EYE:
			programID = _eyeShader->getResourceID();
			break;
		case RenderPasses::LIGHT:
			programID = _lightShader->getResourceID();
			break;
		default:
			programID = 0;
		}

		_textureID[i] = glGetUniformLocation(programID, "VolumeTexture");
		_pvMatrixID[i] = glGetUniformLocation(programID, "PV");
		_invModelMatrixID[i] = glGetUniformLocation(programID, "invM");
		_sizeID[i] = glGetUniformLocation(programID, "size");
		_divRefByCurSamplingRateID[i] = glGetUniformLocation(programID, "divRefByCurSamplingRate");
		_sliceModelID[i] = glGetUniformLocation(programID, "sliceModel");
		_lightTextureID[i] = glGetUniformLocation(programID, "lightTexture");
		_lightPVID[i] = glGetUniformLocation(programID, "lightPV");
	}

	_eyeTextureID = glGetUniformLocation(_passthroughShader->getResourceID(), "eyeTexture");
}

void Material3D::render(
	int playerNumber,
	const glm::mat4 &modelMatrix,
	const glm::mat4 &viewMatrix,
	const glm::mat4 &projectionMatrix)
{
	if (!_texture)
		return;

	vec3 lightDir = SheepEngineMain::getLightDir();
	const vec3 lightColor = SheepEngineMain::getLightColor();
	
	vec3 camPos;
	CameraInfo* camInfo = CameraManager::getCameraInfo(playerNumber);
	if (camInfo)
	{
		camPos = camInfo->_camera->getPosition();
	}
	
	vec3 eye = normalize(vec3(viewMatrix[0][2], viewMatrix[1][2], viewMatrix[2][2]));
	float lightDotEye = dot(lightDir, eye);

	float dirFactor = 1; // back to front
	if (lightDotEye < 0)
	{
		dirFactor = -1; // back to front
	}

	// sampling rate needs to be adjusted with angle
	vec3 bisector = normalize((lightDir + eye * dirFactor) * 0.5f);
	float halfAngle = acos(dot(lightDir, bisector));
	float currentSamplingRate = STANDARD_SAMPLING_RATE * cos(halfAngle);

	vec3 currentNormal(0, 0, 1);
	// rotate current normal to target normal, which is the bisector
	float rotationAngle = acos(dot(currentNormal, bisector));
	vec3 halfAngleRotAxis = normalize(cross(currentNormal, bisector));
	mat4 halfAngleRot = rotate(degrees(rotationAngle), halfAngleRotAxis);

	_lightBuffer->clear(true, false, lightColor.r, lightColor.g, lightColor.b, 1);
	_eyeBuffer->clear(true, false, 0, 0, 0, 1);

	vec3 modelPos = vec3(modelMatrix[3].x, modelMatrix[3].y, modelMatrix[3].z);
	mat4 lightViewMatrix = lookAt(
		modelPos,				// Camera is here
		modelPos + lightDir,	// and looks here
		vec3(0,1,0)				// Head is up (set to 0,-1,0 to look upside-down)
	);
	mat4 lightProjectionMatrix = glm::ortho<float>( -2, 2, -2, 2, -2, 2);
	mat4 modelWithoutRot = removeRotation(modelMatrix);
	mat4 invLightMVP = inverse(lightProjectionMatrix * lightViewMatrix * modelMatrix);

	// update depth buffer for current renderbuffer
	if (camInfo)
	{
		_eyeBuffer->updateRenderTexture(_eyeTexture, camInfo->_renderDepthTexture);
	}

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// do not write into depth buffer
	glDepthMask(GL_FALSE);

	glDisable(GL_CULL_FACE);

	for (float z = 1; z >= -1; z -= (1 / abs(currentSamplingRate)))
	{
		// front to back for similar dir (dotp > 0), else back to front
		mat4 sliceModelMatrix = translate(vec3(modelMatrix[3][0], modelMatrix[3][1], modelMatrix[3][2]));
		sliceModelMatrix = sliceModelMatrix * halfAngleRot;
		sliceModelMatrix = scale(sliceModelMatrix, 0.5f, 0.5f, 0.5f); // need the performance
		sliceModelMatrix = translate(sliceModelMatrix, vec3(0, 0, -z));
		//sliceModelMatrix = scale(sliceModelMatrix, VOLUME_SIZE_FACTOR, VOLUME_SIZE_FACTOR, VOLUME_SIZE_FACTOR); // need the performance
		eyePass(sliceModelMatrix, modelMatrix, viewMatrix, projectionMatrix, z, halfAngleRot, dirFactor, currentSamplingRate, invLightMVP, lightViewMatrix, lightProjectionMatrix);

		// always front to back for light
		lightPass(sliceModelMatrix, modelMatrix, viewMatrix, projectionMatrix, z, halfAngleRot, dirFactor, currentSamplingRate, invLightMVP, lightViewMatrix, lightProjectionMatrix);
	}

	renderEyeBufferToRenderBuffer(playerNumber);

	glEnable(GL_CULL_FACE);

	// write again into depth buffer
	glDepthMask(GL_TRUE);
}


void Material3D::renderZ(
	const glm::mat4 &modelMatrix,
	const glm::mat4 &lightViewMatrix,
	const glm::mat4 &lightProjectionMatrix)
{
}

Material3D::~Material3D()
{
	_eyeShader.reset();
	_modelData.reset();
	_texture.reset();

	delete _lightTexture;
	_lightBuffer.reset();

	delete _eyeTexture;
	delete _eyeBuffer;
}

mat4 Material3D::removeRotation(const mat4 &m)
{
	float scale = sqrt(m[0][0] * m[0][0] + m[1][0] * m[1][0] + m[2][0] * m[2][0]);
	mat4 withoutRotation = mat4(
		scale, 0, 0, 0, // first column (not row!)
		0, scale, 0, 0,
		0, 0, scale, 0,
		m[3][0], m[3][1], m[3][2], 1
	);

	return withoutRotation;
}

void Material3D::eyePass(
	const glm::mat4 &sliceModelMatrix,
	const glm::mat4 &modelMatrix,
	const glm::mat4 &viewMatrix,
	const glm::mat4 &projectionMatrix,
	float sliceZ,
	mat4 &halfAngleRot,
	float dirFactor,
	float currentSamplingRate,
	mat4 &invLightMVP,
	mat4 &lightV,
	mat4 &lightP)
{
	_eyeShader->activate();

	glUniformMatrix4fv(_pvMatrixID[RenderPasses::EYE], 1, GL_FALSE, &(projectionMatrix * viewMatrix)[0][0]);
	glUniformMatrix4fv(_invModelMatrixID[RenderPasses::EYE], 1, GL_FALSE, &inverse(modelMatrix)[0][0]);
	glUniform1f(_sizeID[RenderPasses::EYE], VOLUME_SIZE_FACTOR);
	glUniform1f(_divRefByCurSamplingRateID[RenderPasses::EYE], REFERENCE_SAMPLING_RATE / currentSamplingRate);
	glUniformMatrix4fv(_sliceModelID[RenderPasses::EYE], 1, GL_FALSE, &sliceModelMatrix[0][0]);
	glUniformMatrix4fv(_lightPVID[RenderPasses::EYE], 1, GL_FALSE, &(lightP * lightV)[0][0]);
	
	if (enableBlending)
	{
		if (dirFactor > 0) // back to front
		{
			// over operator
			glEnable(GL_BLEND);
			glBlendFuncSeparate(GL_ONE, GL_ONE_MINUS_SRC_ALPHA, GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);
		}
		else // front to back
		{
			// under operator
			glEnable(GL_BLEND);
			glBlendFuncSeparate(GL_DST_ALPHA, GL_ONE, GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);
		}
	}
	else
	{
		glDisable(GL_BLEND);
	}

	_texture->activate(0);
	glUniform1i(_textureID[RenderPasses::EYE], 0);

	_lightTexture->activate(1);
	glUniform1i(_lightTextureID[RenderPasses::EYE], 1);

	_eyeBuffer->activate();

	_quadVertexBuffer->activate();

	SheepEngineMain::addRenderedVertices(_quadVertexBuffer->getVertices().size());
	if (SheepEngineMain::wireFrameMode == true)
	{
		glDrawArrays(GL_LINES, 0, _quadVertexBuffer->getVertices().size());
	}
	else
	{
		glDrawArrays(GL_TRIANGLES, 0, _quadVertexBuffer->getVertices().size());
	}

	_quadVertexBuffer->deactivate();
	_eyeBuffer->deactivate();
	_texture->deactivate(0);
	_lightTexture->deactivate(1);
	_eyeShader->deactivate();
}

void Material3D::lightPass(
	const glm::mat4 &sliceModelMatrix,
	const glm::mat4 &modelMatrix,
	const glm::mat4 &viewMatrix,
	const glm::mat4 &projectionMatrix,
	float sliceZ,
	mat4 &halfAngleRot,
	float dirFactor,
	float currentSamplingRate,
	mat4 &invLightMVP,
	mat4 &lightV,
	mat4 &lightP)
{
	const vec3 lightDir = SheepEngineMain::getLightDir();
	const vec3 specularColor = SheepEngineMain::getSpecularColor();

	_lightShader->activate();

	// Compute the MVP matrix
	glm::mat4 MVP = projectionMatrix * viewMatrix * modelMatrix;

	glUniformMatrix4fv(_invModelMatrixID[RenderPasses::LIGHT], 1, GL_FALSE, &inverse(modelMatrix)[0][0]);
	glUniform1f(_sizeID[RenderPasses::LIGHT], VOLUME_SIZE_FACTOR);
	glUniform1f(_divRefByCurSamplingRateID[RenderPasses::LIGHT], REFERENCE_SAMPLING_RATE / currentSamplingRate);
	glUniformMatrix4fv(_sliceModelID[RenderPasses::LIGHT], 1, GL_FALSE, &sliceModelMatrix[0][0]);
	glUniformMatrix4fv(_lightPVID[RenderPasses::LIGHT], 1, GL_FALSE, &(lightP * lightV)[0][0]);

	_texture->activate(0);
	glUniform1i(_textureID[RenderPasses::LIGHT], 0);

	_lightBuffer->activate();

	_quadVertexBuffer->activate();

	// always front to back
	// under operator
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	SheepEngineMain::addRenderedVertices(_quadVertexBuffer->getVertices().size());
	glDrawArrays(GL_TRIANGLES, 0, _quadVertexBuffer->getVertices().size());

	_quadVertexBuffer->deactivate();
	_texture->deactivate(0);
	_lightBuffer->deactivate();
	_lightShader->deactivate();
}

void Material3D::renderEyeBufferToRenderBuffer(int playerNumber)
{
	CameraInfo* camInfo = CameraManager::getCameraInfo(playerNumber);
	if (camInfo)
	{
		// Enable normal blending for transparency
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_SRC_ALPHA);

		 camInfo->_renderBuffer->activate();

		_passthroughShader->activate();
		_eyeTexture->activate(0);
		glUniform1i(_eyeTextureID, 0);
		_quadVertexBuffer->activate();

		SheepEngineMain::addRenderedVertices(_quadVertexBuffer->getVertices().size());
		glDrawArrays(GL_TRIANGLES, 0, _quadVertexBuffer->getVertices().size());

		_passthroughShader->deactivate();
		_eyeTexture->deactivate(0);
		_quadVertexBuffer->deactivate();

		camInfo->_renderBuffer->deactivate();
	}
}

void Material3D::switchSamplingMethod(int newMinSamplingMethod, int newMaxSamplingMethod)
{
	if (_texture)
	{
		_texture->switchSamplingMethod(newMinSamplingMethod, newMaxSamplingMethod);
	}
}