#pragma once

#include "PhysicsGameObject.hpp"

#include <glm/glm.hpp>

namespace SheepEngine
{

	class Scarecrow : public PhysicsGameObject
	{
	public:
		Scarecrow(int id, glm::mat4 initialTransform = glm::mat4(), glm::vec3 scale = glm::vec3(1,1,1));
		virtual ~Scarecrow();

		virtual void update(double dt);
	};
}

