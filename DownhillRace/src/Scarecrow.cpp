#include "Scarecrow.hpp"

#include "SheepEngineMain.hpp"
#include "Material.hpp"

#include <algorithm>    // std::max
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>

using namespace std;
using namespace glm;
using namespace SheepEngine;

Scarecrow::Scarecrow(int id, glm::mat4 initialTransform, glm::vec3 scale)
: PhysicsGameObject(scale)
{

	string modelpath;
	string texturepath;

	if (id == 1)
	{
		modelpath = "resources\\models\\scarecrow_body.obj";
		texturepath = "resources\\textures\\scarecrow.bmp";
	}
	if (id == 2)
	{
		modelpath = "resources\\models\\scarecrow_arms.obj";
		texturepath = "resources\\textures\\woodTexture.bmp";
	}
	
	Material *newMaterial = new Material(modelpath,
		texturepath,
		"resources\\shaders\\ShadowMapping.vertexshader",
		"resources\\shaders\\ShadowMapping.fragmentshader");
	_material = newMaterial;

	btTransform tempTransform;
	tempTransform.setFromOpenGLMatrix(glm::value_ptr(initialTransform));
	_rigidBody->setWorldTransform(tempTransform);

	_rigidBody->setLinearFactor(btVector3(0, 0, 0)); // should never move
	_rigidBody->setAngularFactor(btVector3(0, 1, 0));

	// Triangle Collision Shape
	btTriangleMesh *mTriMesh = newMaterial->getTriangleMesh();

	btCollisionShape *mTriMeshShape = new btBvhTriangleMeshShape(mTriMesh, true);
	_rigidBody->setCollisionShape(mTriMeshShape);

	_rigidBody->setMassProps(1, btVector3(1, 1, 1));
}

void Scarecrow::update(double dt)
{
	PhysicsGameObject::update(dt);

	//_rigidBody->applyImpulse(btVector3(10.0f, 10.0f, 0.0f), btVector3(0.0f, 0.0f, 0.0f));
}

Scarecrow::~Scarecrow()
{
}
