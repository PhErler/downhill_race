#pragma once

#include "PhysicsGameObject.hpp"

namespace SheepEngine
{
	class Ground : public PhysicsGameObject
	{
	public:
		Ground();
		virtual ~Ground();
	};
}

