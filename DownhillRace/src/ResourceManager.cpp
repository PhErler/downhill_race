#include "ResourceManager.hpp"

#include "ModelLoader.hpp"

using namespace std;
using namespace glm;
using namespace SheepEngine;

map<string, shared_ptr<Shader>> ResourceManager::s_shaderMap;
map<string, shared_ptr<ModelData>> ResourceManager::s_ModelDataMap;
map<string, shared_ptr<Texture>> ResourceManager::s_textureMap;

ModelData::ModelData(std::vector<glm::vec3>& vertexBuffer, std::vector<glm::vec2>& uvBuffer, std::vector<glm::vec3>& normalBuffer)
: _vertexBuffer(vertexBuffer), _uvBuffer(uvBuffer), _normalBuffer(normalBuffer), _minBB(FLT_MAX, FLT_MAX, FLT_MAX), _maxBB(FLT_MIN, FLT_MIN, FLT_MIN)
{
	for (vec3 &vec : vertexBuffer)
	{
		if (vec.x < _minBB.x)
			_minBB.x = vec.x;
		if (vec.x > _maxBB.x)
			_maxBB.x = vec.x;

		if (vec.y < _minBB.y)
			_minBB.y = vec.y;
		if (vec.y > _maxBB.y)
			_maxBB.y = vec.y;

		if (vec.z < _minBB.z)
			_minBB.z = vec.z;
		if (vec.z > _maxBB.z)
			_maxBB.z = vec.z;
	}
}

shared_ptr<Shader> ResourceManager::getShader(string vertexShaderPath, string fragmentShaderPath, string geometryShaderPath)
{
	string keyString = vertexShaderPath + fragmentShaderPath + geometryShaderPath;

	auto shader = s_shaderMap.find(keyString);
	if (shader == s_shaderMap.end())
	{	// not found
		s_shaderMap[keyString] = std::make_shared<Shader>(vertexShaderPath, fragmentShaderPath, geometryShaderPath);
		return s_shaderMap[keyString];
	}
	else {	// found
		return shader->second;
	}
}

shared_ptr<ModelData> ResourceManager::getModelData(string modelPath)
{
	auto modelData = s_ModelDataMap.find(modelPath);
	if (modelData == s_ModelDataMap.end())
	{	// not found
		vector<glm::vec3> vertices;
		vector<glm::vec2> uvs;
		vector<glm::vec3> normals;
		bool res = ModelLoader::loadOBJ(modelPath.c_str(), vertices, uvs, normals);

		s_ModelDataMap[modelPath] = std::make_shared<ModelData>(vertices, uvs, normals);
		return s_ModelDataMap[modelPath];
	}
	else {	// found
		return modelData->second;
	}
}

shared_ptr<Texture> ResourceManager::getTexture(string texturePath)
{
	auto texture = s_textureMap.find(texturePath);
	if (texture == s_textureMap.end())
	{	// not found
		s_textureMap[texturePath] = std::make_shared<Texture>(texturePath);
		return s_textureMap[texturePath];
	}
	else {	// found
		return texture->second;
	}
}