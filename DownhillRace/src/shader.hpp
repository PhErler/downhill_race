#pragma once

#include "Resource.hpp"

#include <string>

namespace SheepEngine
{

	class Shader : public IResource
	{
	public:
		Shader(std::string vertexShaderPath, std::string fragmentShaderPath, std::string geometryShaderPath = "");
		virtual ~Shader();

		virtual void activate();
		virtual void deactivate();

	private:
		static GLuint LoadShaders(const std::string& vertex_file_path, const std::string& fragment_file_path,
			const std::string& geometryFilePath = nullptr, GLenum gsInput = GL_POINTS, GLenum gsOutput = GL_TRIANGLE_STRIP);

		static std::string Shader::readFile(const std::string& filePath);
	};
}