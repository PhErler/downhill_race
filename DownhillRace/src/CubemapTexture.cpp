#include "CubemapTexture.hpp"

#include <iostream>

//from http://learnopengl.com/code_viewer.php?code=advanced/cubemaps_skybox_optimized

#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))

using namespace std;
using namespace SheepEngine;

static const GLenum types[6] = { 
	GL_TEXTURE_CUBE_MAP_POSITIVE_X,
	GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
	GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
	GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
	GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
	GL_TEXTURE_CUBE_MAP_NEGATIVE_Z 
};

CubemapTexture::CubemapTexture(const string& Directory,
	const string& PosXFilename,
	const string& NegXFilename,
	const string& PosYFilename,
	const string& NegYFilename,
	const string& PosZFilename,
	const string& NegZFilename)
{
	string::const_iterator it = Directory.end();
	it--;
	string BaseDir = (*it == '\\') ? Directory : Directory + "\\";

	m_fileNames[0] = BaseDir + PosXFilename;
	m_fileNames[1] = BaseDir + NegXFilename;
	m_fileNames[2] = BaseDir + PosYFilename;
	m_fileNames[3] = BaseDir + NegYFilename;
	m_fileNames[4] = BaseDir + PosZFilename;
	m_fileNames[5] = BaseDir + NegZFilename;

	_resourceID = 0;

	Load();
}

CubemapTexture::~CubemapTexture()
{
	if (_resourceID != 0) {
		glDeleteTextures(1, &_resourceID);
	}
}

bool CubemapTexture::Load()
{
	vector<char> data;
	int width = 0;
	int height = 0;


	glGenTextures(1, &_resourceID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, _resourceID);

	for (unsigned int i = 0; i < ARRAY_SIZE_IN_ELEMENTS(types); i++) {
		loadBMP(m_fileNames[i], data, width, height);

		glTexImage2D(types[i], 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, &data[0]);

		data.clear();
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	//glBindTexture(GL_TEXTURE_2D, 0);

	return true;
}

void CubemapTexture::activate(unsigned int textureSampler)
{
	activateTextureUnit(textureSampler);
	glBindTexture(GL_TEXTURE_CUBE_MAP, _resourceID);
}

void CubemapTexture::deactivate(unsigned int textureSampler)
{
	//activateTextureUnit(textureSampler);
	//glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}