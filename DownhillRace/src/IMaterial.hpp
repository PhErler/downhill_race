#pragma once

#include <glm/glm.hpp>

namespace SheepEngine
{
	class IMaterial
	{
	public:
		virtual void render(
			int playerNumber,
			const glm::mat4 &modelMatrix, 
			const glm::mat4 &viewMatrix, 
			const glm::mat4 &projectionMatrix) = 0;

		// z-buffer pass for shadow mapping
		virtual void renderZ(
			const glm::mat4 &modelMatrix,
			const glm::mat4 &lightViewMatrix,
			const glm::mat4 &lightProjectionMatrix) = 0;

		virtual void switchSamplingMethod(int newMinSamplingMethod, int newMaxSamplingMethod){};
	};
}

