#include "Texture3D.hpp"

#include "glm\glm.hpp"

using namespace std;
using namespace glm;
using namespace SheepEngine;

// basing on: http://content.gpwiki.org/index.php/OpenGL:Tutorials:3D_Textures

namespace
{
	const int BYTES_PER_TEXEL = 4;
}

Texture3D::Texture3D(int playerNumber)
{
	tex3D tex;

	//createWindowTestTexture(tex);
	//createBricksTestTexture(tex);
	//createSlicesTestTexture(tex);
	//createShadowTestTexture(tex);
	createWoolTexture(tex, playerNumber);

	// ask for enough memory for the texels and make sure we got it before proceeding
	int width = tex.size();
	int height = tex[0].size();
	int depth = tex[0][0].size();
	vector<unsigned char> texels(width * height * depth * BYTES_PER_TEXEL);

#define LAYER(r)	(width * height * r * BYTES_PER_TEXEL)
	// 2->1 dimension mapping function
#define TEXEL2(s, t)	(BYTES_PER_TEXEL * (s * width + t))
	// 3->1 dimension mapping function
#define TEXEL3(s, t, r)	(TEXEL2(s, t) + LAYER(r))

	for (int x = 0; x < width; ++x)
	{
		for (int y = 0; y < height; ++y)
		{
			for (int z = 0; z < depth; ++z)
			{
				vec4 color = tex[x][y][z];
				texels[TEXEL3(x, y, z) + 0] = static_cast<unsigned char>(color.r * 255.0f);
				texels[TEXEL3(x, y, z) + 1] = static_cast<unsigned char>(color.g * 255.0f);
				texels[TEXEL3(x, y, z) + 2] = static_cast<unsigned char>(color.b * 255.0f);
				texels[TEXEL3(x, y, z) + 3] = static_cast<unsigned char>(color.a * 255.0f);
			}
		}
	}

	// request 1 texture name from OpenGL
	glGenTextures(1, &_resourceID);
	// tell OpenGL we're going to be setting up the texture name it gave us	
	glBindTexture(GL_TEXTURE_3D, _resourceID);
	// when this texture needs to be shrunk to fit on small polygons, use linear interpolation of the texels to determine the color
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	// when this texture needs to be magnified to fit on a big polygon, use linear interpolation of the texels to determine the color
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// we want the texture to repeat over the S axis, so if we specify coordinates out of range we still get textured.
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	// same as above for T axis
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	// same as above for R axis
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP);
	// this is a 3d texture, level 0 (max detail), GL should store it in RGB8 format, its WIDTHxHEIGHTxDEPTH in size, 
	// it doesnt have a border, we're giving it to GL in RGB format as a series of unsigned bytes, and texels is where the texel data is.
	glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA8, width, height, depth, 0, GL_RGBA, GL_UNSIGNED_BYTE, &texels[0]);

	//glBindTexture(GL_TEXTURE_3D, 0);
}

Texture3D::~Texture3D()
{
}

void Texture3D::activate(unsigned int textureSampler)
{
	activateTextureUnit(textureSampler);
	glBindTexture(GL_TEXTURE_3D, _resourceID);
}

void Texture3D::deactivate(unsigned int textureSampler)
{
	//activateTextureUnit(textureSampler);
	//glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture3D::createWindowTestTexture(tex3D &tex)
{
	const int WIDTH = 10;
	const int HEIGHT = 10;
	const int DEPTH = 10;

	tex = tex3D(WIDTH);
	for (int x = 0; x < WIDTH; ++x)
	{
		tex[x] = vector<vector<vec4>>(HEIGHT);
		for (int y = 0; y < HEIGHT; ++y)
		{
			tex[x][y] = vector<vec4>(DEPTH);
			for (int z = 0; z < DEPTH; ++z)
			{
				if (x == 0 || x == WIDTH - 1 || y == 0 || y == HEIGHT - 1/* || z == 0 || z == DEPTH - 1*/)
				{
					tex[x][y][z] = vec4(0, 0, 0, 0.9f);
				}
				else
				{
					tex[x][y][z] = vec4(1, 1, 1, 0.1f);
				}
			}
		}
	}
}

void Texture3D::createBricksTestTexture(tex3D &tex)
{
	const int WIDTH = 5;
	const int HEIGHT = 5;
	const int DEPTH = 5;

	tex = tex3D(WIDTH);
	for (int x = 0; x < WIDTH; ++x)
	{
		tex[x] = vector<vector<vec4>>(HEIGHT);
		for (int y = 0; y < HEIGHT; ++y)
		{
			tex[x][y] = vector<vec4>(DEPTH);
			for (int z = 0; z < DEPTH; ++z)
			{
				tex[x][y][z] = vec4(float(x) / float(WIDTH),
					float(y) / float(HEIGHT),
					float(z) / float(DEPTH),
					0.2f);
			}
		}
	}

	// lines through cube
	for (int x = 0; x < WIDTH; ++x)
	{
		tex[x][0][0] = vec4(1.0f, 0.0f, 0.0f, 0.8f);
	}
	for (int y = 0; y < HEIGHT; ++y)
	{
		tex[1][y][1] = vec4(0.0f, 1.0f, 0.0f, 0.8f);
	}
	for (int z = 0; z < DEPTH; ++z)
	{
		tex[2][2][z] = vec4(0.0f, 0.0f, 1.0f, 0.8f);
	}
}

void Texture3D::createSlicesTestTexture(tex3D &tex)
{
	const int WIDTH = 10;
	const int HEIGHT = 10;
	const int DEPTH = 10;

	tex = tex3D(WIDTH);
	for (int x = 0; x < WIDTH; ++x)
	{
		tex[x] = vector<vector<vec4>>(HEIGHT);
		for (int y = 0; y < HEIGHT; ++y)
		{
			tex[x][y] = vector<vec4>(DEPTH);
			for (int z = 0; z < DEPTH; ++z)
			{
				if (z % 2 == 0)
				{
					tex[x][y][z] = vec4(0, 0, 0, 0.9f);
				}
				else
				{
					tex[x][y][z] = vec4(1, 1, 1, 0.1f);
				}
			}
		}
	}
}

void Texture3D::createShadowTestTexture(tex3D &tex)
{
	const int WIDTH = 10;
	const int HEIGHT = 10;
	const int DEPTH = 10;

	tex = tex3D(WIDTH);
	for (int x = 0; x < WIDTH; ++x)
	{
		tex[x] = vector<vector<vec4>>(HEIGHT);
		for (int y = 0; y < HEIGHT; ++y)
		{
			tex[x][y] = vector<vec4>(DEPTH);
			for (int z = 0; z < DEPTH; ++z)
			{
				if (x == WIDTH - 2 && (y > HEIGHT / 4 && y < HEIGHT * 3 / 4)
								   && (z > DEPTH / 4 && z < DEPTH * 3 / 4))
				{
					tex[x][y][z] = vec4(0.5f, 0.5f, 1.0f, 1.0f);
				}
				else if (x == 1)
				{
					tex[x][y][z] = vec4(1.0f, 0.5f, 0.5f, 1.0f);
				}
				else
				{
					tex[x][y][z] = vec4(0.25f, 0.5f, 0.5f, 0.1f);
				}
			}
		}
	}
}

void Texture3D::createWoolTexture(tex3D &tex, int playerNumber)
{
	const int WIDTH = 100;
	const int HEIGHT = 100;
	const int DEPTH = 100;

	const float InnerBorderDist = 0.3f;
	const float MiddleBorderDist = 0.4f;

	vec3 color(0.5f, 0.5f, 0.5f);
	if (playerNumber == 0)
	{
		color = vec3(0.8f, 0.8f, 0.8f); // white
	}
	else if (playerNumber == 1)
	{
		color = vec3(0.2f, 0.2f, 0.2f); // black
	}
	else if (playerNumber == 2)
	{
		color = vec3(0.5f, 0.2f, 0.2f); // red
	}
	else if (playerNumber == 3)
	{
		color = vec3(0.4f, 0.3f, 0.0f); // brown
	}

	// shells of increasing opacity to the center
	tex = tex3D(WIDTH);
	for (int x = 0; x < WIDTH; ++x)
	{
		tex[x] = vector<vector<vec4>>(HEIGHT);
		for (int y = 0; y < HEIGHT; ++y)
		{
			tex[x][y] = vector<vec4>(DEPTH);
			for (int z = 0; z < DEPTH; ++z)
			{
				float distX = abs(float(x) - float(WIDTH) / 2) / float(WIDTH) * 2;
				float distY = abs(float(y) - float(HEIGHT) / 2) / float(HEIGHT) * 2;
				float distZ = abs(float(z) - float(DEPTH) / 2) / float(DEPTH) * 2;
				float distToCenter = sqrtf(distX*distX + distY*distY + distZ*distZ);

				if (distToCenter < InnerBorderDist)
				{
					float randomFactor = float(rand() % 1000) * 0.001f * 0.2f;
					tex[x][y][z] = vec4(color.r, color.g, color.b, 0.6f + randomFactor);
				}
				else if (distToCenter < MiddleBorderDist)
				{
					float linearFactor = (distToCenter - InnerBorderDist) / (MiddleBorderDist - InnerBorderDist);
					float randomFactor = float(rand() % 1000) * 0.001f * 0.2f;
					tex[x][y][z] = vec4(color.r, color.g, color.b, 0.6f - linearFactor * 0.6f + randomFactor);
				}
				else
				{
					tex[x][y][z] = vec4(color.r, color.g, color.b, 0.0f);
				}
			}
		}
	}
}

void Texture3D::switchSamplingMethod(int newMinSamplingMethod, int newMaxSamplingMethod)
{
	glBindTexture(GL_TEXTURE_3D, _resourceID);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, newMinSamplingMethod);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, newMaxSamplingMethod);
	//glBindTexture(GL_TEXTURE_3D, 0);
}