#pragma once

#include "GameObject.hpp"

#include <glm/glm.hpp>

namespace SheepEngine
{

	class Leg : public GameObject
	{
	public:
		Leg(glm::vec3 rotAxis);
		virtual void update(double dt);

	protected:
		glm::vec3 _rotAxis;
	};
}

