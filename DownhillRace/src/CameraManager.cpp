#include "CameraManager.hpp"

#include "Camera.hpp"
#include "ResourceManager.hpp"
#include "SheepEngineMain.hpp"

using namespace std;
using namespace glm;
using namespace SheepEngine;

std::vector<CameraInfo> CameraManager::s_cameraInfo = std::vector<CameraInfo>();

// render to framebuffer
shared_ptr<Shader> CameraManager::s_passthroughShader = nullptr;
GLuint CameraManager::s_renderTextureID = 0;

CameraInfo::CameraInfo(Camera* camera,
	glm::mat4 viewMatrix,
	glm::mat4 projectionMatrix,
	int player)
	: _camera(camera)
	, _viewMatrix(viewMatrix)
	, _projectionMatrix(projectionMatrix)
	, _player(player)
{
	float leftNDC = -1;
	float rightNDC = 1;
	float topNDC = 1;
	float bottomNDC = -1;

	vector<vec3> vertices; // plane made of 2 triangles
	vertices.push_back(vec3(leftNDC, bottomNDC, 0.0f));
	vertices.push_back(vec3(rightNDC, bottomNDC, 0.0f));
	vertices.push_back(vec3(leftNDC, topNDC, 0.0f));
	vertices.push_back(vec3(leftNDC, topNDC, 0.0f));
	vertices.push_back(vec3(rightNDC, bottomNDC, 0.0f));
	vertices.push_back(vec3(rightNDC, topNDC, 0.0f));
	_quadVertexBuffer = new VertexBuffer(vertices);

	int xRes = 0;
	int yRes = 0;
	CameraManager::calcRenderbufferSize(xRes, yRes);

	_renderColorTexture = new RenderTexture(xRes, yRes, RenderTextureType::RGBA);
	_renderDepthTexture = new RenderTexture(xRes, yRes, RenderTextureType::Depth);
	_renderBuffer = new FrameBuffer(_renderColorTexture, _renderDepthTexture);
}

void CameraManager::init()
{
	s_passthroughShader = ResourceManager::getShader(
		"resources\\shaders\\Passthrough.vertexshader",
		"resources\\shaders\\Passthrough.fragmentshader");

	s_renderTextureID = glGetUniformLocation(s_passthroughShader->getResourceID(), "eyeTexture");
}

unsigned int CameraManager::getMaxCameraIndex()
{
	return s_cameraInfo.size() - 1;
}

unsigned int CameraManager::addCamera(Camera* camera)
{
	s_cameraInfo.push_back(CameraInfo(camera, glm::mat4(), glm::mat4(), s_cameraInfo.size()));
	return getMaxCameraIndex();
}

void CameraManager::removeCamera(unsigned int cameraIndex)
{
	if (cameraIndex >= 0 && cameraIndex < s_cameraInfo.size())
	{
		auto itCamera = s_cameraInfo.begin();
		for (unsigned int i = 0; i < cameraIndex; ++i)
		{
			++itCamera;
		}
		s_cameraInfo.erase(itCamera);
	}
}

CameraInfo* CameraManager::getCameraInfo(unsigned int cameraIndex)
{
	if (cameraIndex >= 0 && cameraIndex < s_cameraInfo.size())
	{
		return &s_cameraInfo[cameraIndex];
	}
	else
	{
		return nullptr;
	}
}

void CameraManager::setViewMatrix(unsigned int cameraIndex, glm::mat4 newViewMatrix)
{
	if (cameraIndex >= 0 && cameraIndex < s_cameraInfo.size())
	{
		s_cameraInfo[cameraIndex]._viewMatrix = newViewMatrix;
	}
}

void CameraManager::setProjectionMatrix(unsigned int cameraIndex, glm::mat4 newProjectionMatrix)
{
	if (cameraIndex >= 0 && cameraIndex < s_cameraInfo.size())
	{
		s_cameraInfo[cameraIndex]._projectionMatrix = newProjectionMatrix;
	}
}

void CameraManager::renderToScreen()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0); // unbind all framebuffers

	// Clear the screen
	glClearColor(0, 0, 0.4f, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// don't need transparency
	glDisable(GL_BLEND);

	s_passthroughShader->activate();

	int playerNumber = 0;
	for (CameraInfo& camInfo : s_cameraInfo)
	{
		int left;
		int top;
		int width;
		int height;
		calcBordersForScreen(playerNumber, left, top, width, height);
		SheepEngineMain::setViewPort(Rect(left, top, width, height));

		camInfo._renderColorTexture->activate(0);
		glUniform1i(s_renderTextureID, 0);
		camInfo._quadVertexBuffer->activate();

		glDrawArrays(GL_TRIANGLES, 0, camInfo._quadVertexBuffer->getVertices().size());

		camInfo._renderColorTexture->deactivate(0);
		camInfo._quadVertexBuffer->deactivate();

		++playerNumber;
	}

	s_passthroughShader->deactivate();
}

void CameraManager::calcBordersForNDC(int player, float &top_out, float &bottom_out, float &left_out, float &right_out)
{
	int numPlayers = SheepEngineMain::getWindowParams()._numPlayers;
	assert(player >= 0 /*&& player < numPlayers*/);

	if (numPlayers == 1)
	{
		if (player == 0)
		{
			top_out = 1;
			bottom_out = -1;
			left_out = -1;
			right_out = 1;
		}
	}
	if (numPlayers == 2) // horizontal split
	{
		if (player == 0)
		{
			top_out = 1;
			bottom_out = 0;
			left_out = -1;
			right_out = 1;
		}
		else if (player == 1)
		{
			top_out = 0;
			bottom_out = -1;
			left_out = -1;
			right_out = 1;
		}
	}
	if (numPlayers >= 3)
	{
		if (player == 0)
		{
			top_out = 1;
			bottom_out = 0;
			left_out = -1;
			right_out = 0;
		}
		else if (player == 1)
		{
			top_out = 1;
			bottom_out = 0;
			left_out = 0;
			right_out = 1;
		}
		else if (player == 2)
		{
			top_out = 0;
			bottom_out = -1;
			left_out = -1;
			right_out = 0;
		}
		else if (player == 3)
		{
			top_out = 0;
			bottom_out = -1;
			left_out = 0;
			right_out = 1;
		}
	}
}

void CameraManager::calcBordersForScreen(int player, int &left_out, int &top_out, int &width_out, int &height_out)
{
	int numPlayers = SheepEngineMain::getWindowParams()._numPlayers;
	int xRes = SheepEngineMain::getWindowParams()._xRes;
	int yRes = SheepEngineMain::getWindowParams()._yRes;

	float left = 0;
	float right = 0;
	float top = 0;
	float bottom = 0;

	calcBordersForNDC(player, top, bottom, left, right);

	left_out = int((left / 2 + 0.5f) * xRes);
	top_out = int((1 - (top / 2 + 0.5f)) * yRes);
	int rightPx = int((right / 2 + 0.5f) * xRes);
	int bottomPx = int((1 - (bottom / 2 + 0.5f)) * yRes);
	height_out = bottomPx - top_out;
	width_out = rightPx - left_out;
}

void CameraManager::calcRenderbufferSize(int &width_out, int &height_out)
{
	int numPlayers = SheepEngineMain::getWindowParams()._numPlayers;
	int xRes = SheepEngineMain::getWindowParams()._xRes;
	int yRes = SheepEngineMain::getWindowParams()._yRes;

	int divFactorX = 1;
	int divFactorY = 1;

	if (numPlayers == 2)
	{
		divFactorY = 2;
	}
	else if (numPlayers == 2)
	{
		divFactorX = 2;
		divFactorY = 2;
	}

	width_out = xRes / divFactorX;
	height_out = yRes / divFactorY;
}