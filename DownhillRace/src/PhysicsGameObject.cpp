#include "PhysicsGameObject.hpp"

#include "SheepEngineMain.hpp"
#include "MotionState.hpp"
#include "ResourceManager.hpp"

#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace std;
using namespace glm;
using namespace SheepEngine;

PhysicsGameObject::PhysicsGameObject(glm::vec3 scale)
: _scale(scale)
{
	// about collision shapes: https://www.panda3d.org/manual/index.php/Bullet_Collision_Shapes
	_collisionShape = new btSphereShape(length(_scale)); // place holder

	btTransform tempTransform;
	tempTransform.setFromOpenGLMatrix(glm::value_ptr(mat4()));
	_motionState = new MotionState(this);

	btRigidBody::btRigidBodyConstructionInfo
		rigidBodyCI(1, _motionState, _collisionShape, btVector3(0, 0, 0));

	_rigidBody = new btRigidBody(rigidBodyCI);

	SheepEngineMain::getPhysicsWorld()->addRigidBody(_rigidBody, 1, 1);
}

PhysicsGameObject::~PhysicsGameObject()
{
	SheepEngineMain::getPhysicsWorld()->removeRigidBody(_rigidBody);
	delete _collisionShape;
	delete _motionState;
	delete _rigidBody;
}