#pragma once

#include "PhysicsGameObject.hpp"

#include <glm/glm.hpp>

namespace SheepEngine
{

	class Barrel : public PhysicsGameObject
	{
	public:
		Barrel(glm::mat4 initialTransform = glm::mat4(), glm::vec3 scale = glm::vec3(1, 1, 1));
		virtual ~Barrel();
	};
}

