#pragma once

#include "GameObject.hpp"

#include "shader.hpp"
#include "texture.hpp"

#include <vector>
#include <memory>

namespace SheepEngine
{
	struct Particle{
		glm::vec3 position;
		glm::vec4 color;
		float velocity;
		float size;
		float life;
		float cameradistance; // distance to camera

		bool operator<(const Particle& that) const {
			// Sort in reverse order : far particles drawn first.
			return this->cameradistance > that.cameradistance;
		}
	};

	class ParticleEmitter : public GameObject
	{
	public:
		ParticleEmitter();
		virtual ~ParticleEmitter();

		virtual void update(double dt);
		virtual void render(int playerCam);
		virtual void renderZ();

		static bool enableBlending;

	private:
		std::vector<Particle> _particles;

		int _lastParticle = 0;
		int _particleCount = 0;

		float _particleToSpawn = 0;

		std::vector<GLfloat> _particle_position;
		std::vector<GLubyte> _particle_color;

		std::shared_ptr<Texture> texture;
		std::shared_ptr<Shader> program;

		GLuint VertexArrayID;
		GLuint TextureID;

		GLuint CameraRight_worldspace_ID;
		GLuint CameraUp_worldspace_ID;
		GLuint ViewProjMatrixID;

		GLuint _billboard_vertex_buffer;
		GLuint _particles_position_buffer;
		GLuint _particles_color_buffer;

		int getNextParticleIndex();
		void sortParticles();
		void addParticle();
	};
}

