#pragma once

#include "Resource.hpp"
#include "Texture.hpp"
#include "Shader.hpp"
#include "ResourceManager.hpp"
#include "IMaterial.hpp"

#include <vector>
#include <string>

#include <GL/glew.h>

#include <glm/glm.hpp>

#include <btBulletDynamicsCommon.h>

namespace SheepEngine
{
	class Material : public IMaterial
	{
	public:
		Material(
			std::string modelPath = std::string(),
			std::string texturePath = std::string(),
			std::string vertexShaderPath = std::string(),
			std::string fragmentShaderPath = std::string());
		virtual ~Material();

		virtual void render(
			int playerNumber,
			const glm::mat4 &modelMatrix, 
			const glm::mat4 &viewMatrix, 
			const glm::mat4 &projectionMatrix);

		// z-buffer pass for shadow mapping
		virtual void renderZ(
			const glm::mat4 &modelMatrix,
			const glm::mat4 &lightViewMatrix,
			const glm::mat4 &lightProjectionMatrix);

		bool isVisible(const glm::mat4 &MVP);

		std::shared_ptr<ModelData> getModelData() { return _modelData; }

		void setTextureTiling(glm::vec2 tilingFactors) { _textureTiling = tilingFactors; }

		btTriangleMesh* getTriangleMesh();

		virtual void switchSamplingMethod(int newMinSamplingMethod, int newMaxSamplingMethod);

	protected:
		VertexArray _vertexArray;
		std::shared_ptr<Shader> _shader;
		std::shared_ptr<ModelData> _modelData;
		std::shared_ptr<Texture> _texture;
		glm::vec2 _textureTiling = glm::vec2(1, 1);

		GLuint _textureID = 0;
		GLuint _mvpMatrixID = 0;
		GLuint _viewMatrixID = 0;
		GLuint _modelMatrixID = 0;
		GLuint _lightBiasMVPID = 0;
		GLuint _invLightDirID = 0;
		GLuint _lightColorID = 0;
		GLuint _ambientColorID = 0;
		GLuint _specularColorID = 0;
		GLuint _textureTilingID = 0;
		GLuint _shadowMapID = 0;
		GLuint _shadowMappingSampleDistanceID = 0;
	};
}

