#pragma once

#include "Resource.hpp"

namespace SheepEngine
{
	namespace RenderTextureType
	{
		enum Enum
		{
			RGB,
			RGBA,
			Depth
		};
	}

	class RenderTexture : public ITexture
	{
	public:
		RenderTexture(GLuint width, GLuint height, RenderTextureType::Enum type);
		virtual ~RenderTexture();

		virtual void activate(unsigned int textureSampler);
		virtual void deactivate(unsigned int textureSampler);

		GLuint getWidth(){ return _width; }
		GLuint getHeight(){ return _height; }

	protected:
		GLuint _width = 0;
		GLuint _height = 0;
	};
}
