#include "ParticleEmitter.hpp"

#include "SheepEngineMain.hpp"
#include "ResourceManager.hpp"
#include "CameraManager.hpp"
#include "Camera.hpp"
#include "PhysicsGameObject.hpp"

#include <algorithm>

#include <glm/gtx/norm.hpp>

using namespace std;
using namespace glm;
using namespace SheepEngine;

bool ParticleEmitter::enableBlending = true;

namespace
{
	const int MAX_PARTICLES = 25;

	const GLfloat vertex_buffer[] = {
		-0.5f, -0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		-0.5f, 0.5f, 0.0f,
		0.5f, 0.5f, 0.0f,
	};
}

ParticleEmitter::ParticleEmitter()
: _particles(MAX_PARTICLES)
, _particle_position(MAX_PARTICLES * 4)
, _particle_color(MAX_PARTICLES * 4)
{
	VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	program = ResourceManager::getShader(
		"resources\\shaders\\Particle.vertexshader",
		"resources\\shaders\\Particle.fragmentshader");

	// Vertex shader
	CameraRight_worldspace_ID = glGetUniformLocation(program->getResourceID(), "CameraRight_worldspace");
	CameraUp_worldspace_ID = glGetUniformLocation(program->getResourceID(), "CameraUp_worldspace");
	ViewProjMatrixID = glGetUniformLocation(program->getResourceID(), "VP");

	// fragment shader
	TextureID = glGetUniformLocation(program->getResourceID(), "myTextureSampler");

	for (int i = 0; i < MAX_PARTICLES; i++){
		_particles[i].life = -1.0f;
		_particles[i].cameradistance = -1.0f;
	}

	// particle texture
	texture = make_shared<Texture>(string("resources\\textures\\dustParticle.DDS"));

	glGenBuffers(1, &_billboard_vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, _billboard_vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_buffer), vertex_buffer, GL_STATIC_DRAW);


	// VBO containing positions and sizes of particles
	glGenBuffers(1, &_particles_position_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, _particles_position_buffer);
	glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * 4 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);

	// VBO containing colors of particles
	glGenBuffers(1, &_particles_color_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, _particles_color_buffer);
	glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * 4 * sizeof(GLubyte), NULL, GL_STREAM_DRAW);
}

void ParticleEmitter::update(double dt)
{
	GameObject::update(dt);

	// add new particles
	float velocityFactor = 0.5f;
	if (_parent)
	{
		const float VelThreshold = 15;
		const float VelMax = 150;
		btRigidBody *parentRB = static_cast<PhysicsGameObject*>(_parent)->getRigidBody();
		float velocitySquared = parentRB->getLinearVelocity().length2();
		velocityFactor = (velocitySquared - VelThreshold) / (VelMax - VelThreshold);
		velocityFactor = clamp<float>(velocityFactor, 0, 1);
	}
	_particleToSpawn += float((rand() % 40 + 10) * dt) * velocityFactor;
	int newParticles = int(_particleToSpawn);
	for (int i = 0; i < newParticles; i++)
	{
		addParticle();
	}

	// update particles
	_particleCount = 0;
	for (int i = 0; i < MAX_PARTICLES; ++i)
	{
		Particle& p = _particles[i];

		// decrease life, transparency and size
		p.life -= float(dt);
		p.color.a -= 25 * static_cast<unsigned char>(dt);
		p.size -= 0.05f * float(dt);

		if (p.life > 0 || p.color.a > 0 || p.size > 0)
		{
			// random direction
			float x = (rand() % 10) / 10.0f;
			float y = (rand() % 2) / 10.0f;
			float z = (rand() % 10) / 10.0f;
			if (rand() % 2) x *= (-1);
			if (rand() % 2) z *= (-1);
			p.position += vec3(x, y, z) * p.velocity;

			vec3 CameraPosition;
			CameraInfo* camInfo = CameraManager::getCameraInfo(0);
			if (camInfo)
			{
				CameraPosition = camInfo->_camera->getPosition();
			}
			p.cameradistance = glm::length2(p.position - CameraPosition);

			// Fill the GPU buffer
			_particle_position[4 * _particleCount + 0] = p.position.x;
			_particle_position[4 * _particleCount + 1] = p.position.y;
			_particle_position[4 * _particleCount + 2] = p.position.z;
			_particle_position[4 * _particleCount + 3] = p.size;

			_particle_color[4 * _particleCount + 0] = unsigned char(p.color.r);
			_particle_color[4 * _particleCount + 1] = unsigned char(p.color.g);
			_particle_color[4 * _particleCount + 2] = unsigned char(p.color.b);
			_particle_color[4 * _particleCount + 3] = unsigned char(p.color.a);
		}
		else
		{
			// Particles that just died will be put at the end of the buffer in SortParticles();
			p.cameradistance = -1.0f;
		}
		++_particleCount;
	}

	//sortParticles(); // currently not needed
}

void ParticleEmitter::render(int playerCam)
{
	GameObject::render(playerCam);

	CameraInfo* camInfo = CameraManager::getCameraInfo(playerCam);
	if (camInfo)
	{
		camInfo->_renderBuffer->activate();

		glBindBuffer(GL_ARRAY_BUFFER, _particles_position_buffer);
		glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * 4 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, _particleCount * sizeof(GLfloat)* 4, &_particle_position[0]);

		glBindBuffer(GL_ARRAY_BUFFER, _particles_color_buffer);
		glBufferData(GL_ARRAY_BUFFER, MAX_PARTICLES * 4 * sizeof(GLubyte), NULL, GL_STREAM_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, _particleCount * sizeof(GLubyte)* 4, &_particle_color[0]);

		if (enableBlending)
		{
			glEnable(GL_BLEND);
			glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA); // we don't want the particle to change our renderbuffer's alpha
		}
		else
		{
			glDisable(GL_BLEND);
		}

		glDepthMask(GL_FALSE);

		// Use our shader
		program->activate();

		// Bind our texture in Texture Unit 0
		texture->activate(0);
		glUniform1i(TextureID, 0);

		// particles face camera
		mat4 ProjectionMatrix = camInfo->_projectionMatrix;
		mat4 ViewMatrix = camInfo->_viewMatrix;
		mat4 ViewProjectionMatrix = ProjectionMatrix * ViewMatrix;
		glUniform3f(CameraRight_worldspace_ID, ViewMatrix[0][0], ViewMatrix[1][0], ViewMatrix[2][0]);
		glUniform3f(CameraUp_worldspace_ID, ViewMatrix[0][1], ViewMatrix[1][1], ViewMatrix[2][1]);

		glUniformMatrix4fv(ViewProjMatrixID, 1, GL_FALSE, &ViewProjectionMatrix[0][0]);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, _billboard_vertex_buffer);
		glVertexAttribPointer(
			0,                  // attribute
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);

		// 2nd attribute buffer : positions of particles' centers
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, _particles_position_buffer);
		glVertexAttribPointer(
			1,                               // attribute
			4,                                // size : x + y + z + size => 4
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// 3rd attribute buffer : particles' colors
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, _particles_color_buffer);
		glVertexAttribPointer(
			2,                                // attribute
			4,                                // size : r + g + b + a => 4
			GL_UNSIGNED_BYTE,                 // type
			GL_TRUE,                          // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		//glVertexAttribDivisor(0, 0); // particles vertices
		glVertexAttribDivisor(1, 1); // position
		glVertexAttribDivisor(2, 1); // color

		// draw particles
		SheepEngineMain::addRenderedVertices(_particleCount);
		glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, _particleCount);

		// restore / unbind
		glDepthMask(GL_TRUE);

		//glVertexAttribDivisor(0, 0);
		glVertexAttribDivisor(1, 0);
		glVertexAttribDivisor(2, 0);

		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		texture->deactivate(0);
		program->deactivate();

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);

		camInfo->_renderBuffer->deactivate();
	}
}

void ParticleEmitter::renderZ()
{
	//GameObject::renderZ();
}

ParticleEmitter::~ParticleEmitter()
{
	_particle_position.clear();

	// Cleanup VBO and shader
	glDeleteBuffers(1, &_particles_color_buffer);
	glDeleteBuffers(1, &_particles_position_buffer);
	glDeleteBuffers(1, &_billboard_vertex_buffer);
	program.reset();
	texture.reset();
	glDeleteVertexArrays(1, &VertexArrayID);
}


int ParticleEmitter::getNextParticleIndex(){

	for (int i = _lastParticle; i < MAX_PARTICLES; i++)
	{
		if (_particles[i].life < 0 || _particles[i].color.a < 0 || _particles[i].size < 0){
			_lastParticle = i;
			return i;
		}
	}
	for (int i = 0; i < _lastParticle; i++)
	{
		if (_particles[i].life < 0 || _particles[i].color.a < 0 || _particles[i].size < 0){
			_lastParticle = i;
			return i;
		}
	}
	return 0;
}

void ParticleEmitter::sortParticles(){
	sort(_particles.begin(), _particles.end());
}

void ParticleEmitter::addParticle()
{
	int index = getNextParticleIndex();

	_particles[index].position = getPosition();
	_particles[index].life = float(rand() % 5 + 1);	
	_particles[index].velocity = (rand() % 2 + 1) / 10.0f;

	// random brownish color
	/*_particles[index].color.r = 225;
	_particles[index].color.g = rand() % 80 + 100;
	_particles[index].color.b = rand() % 150 + 5;
	_particles[index].color.a = 150;*/

	_particles[index].color = vec4(206, 176, 140, 125);
	_particles[index].size = (rand() % 1000) / 2000.0f + 0.1f;
	
	_particleToSpawn -= 1;
}