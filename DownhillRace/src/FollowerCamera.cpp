#include "FollowerCamera.hpp"

#include "SheepEngineMain.hpp"
#include "CameraManager.hpp"

#include "glm\gtc\matrix_transform.hpp"

using namespace std;
using namespace glm;
using namespace SheepEngine;

namespace
{
	const float CamDistance = 5;
	const float CamTurnThreshold = 2.5f;
	const float HeightOffset = 1.5f;
}

FollowerCamera::FollowerCamera(PhysicsGameObject* followThis)
: _followThis(followThis)
{
	if (_followThis) // init behind and above followed object
	{
		mat4 followThisMatrix = _followThis->getModelMatrix();
		followThisMatrix[3] = vec4(0, 0, 0, 1);
		vec3 dir = mat3(followThisMatrix) * vec3(0, 0, 1);
		dir = glm::normalize(dir);
		glm::vec3 up = glm::vec3(0, 1, 0);

		vec3 targetPos = _followThis->getPosition();
		_position = targetPos - dir * CamDistance + up * HeightOffset;

		// Camera matrix
		mat4 viewMatrix = glm::lookAt(
			_position,				// Camera is here
			targetPos,				// and looks here
			up						// Head is up (set to 0,-1,0 to look upside-down)
			);

		CameraManager::setViewMatrix(_cameraIndex, viewMatrix);

		_modelMatrix[3][0] = _position.x;
		_modelMatrix[3][1] = _position.y;
		_modelMatrix[3][2] = _position.z;
		_modelMatrix[3][3] = 1;
	}
}

void FollowerCamera::update(double dt)
{
	if (_followThis)
	{
		vec3 targetPos = _followThis->getPosition();
		glm::vec3 up = glm::vec3(0, 1, 0);
		vec3 dir;

		float dampingFactor = 0.3f;
		vec3 lastPos = _position;

		dir = targetPos - _position;
		dir.y = 0;
		dir = glm::normalize(dir);

		vec3 newPos = (targetPos - dir * CamDistance) + up * HeightOffset;
		vec3 dampedPos = newPos * dampingFactor + lastPos * (1 - dampingFactor);

		_position = dampedPos;

		// Camera matrix
		mat4 viewMatrix = glm::lookAt(
			_position,				// Camera is here
			targetPos,				// and looks here
			up						// Head is up (set to 0,-1,0 to look upside-down)
			);

		CameraManager::setViewMatrix(_cameraIndex, viewMatrix);

		_modelMatrix[3][0] = _position.x;
		_modelMatrix[3][1] = _position.y;
		_modelMatrix[3][2] = _position.z;
		_modelMatrix[3][3] = 1;
	}
}