#include "Sheep.hpp"

#include "SheepEngineMain.hpp"
#include "DRGameManager.hpp"
#include "Camera.hpp"
#include "Material.hpp"
#include "Material3D.hpp"
#include "Leg.hpp"

#include <algorithm>    // std::max
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>

using namespace std;
using namespace glm;
using namespace SheepEngine;

namespace
{
	//const btScalar AccelerationImpulse = 1250;
	//const btScalar AccelerationImpulse = 1000;
	const btScalar AccelerationImpulse = 850;
	const btScalar LinearDamping = 0.5f;
	//const btScalar Gravity = 13;
	//const btScalar Gravity = 17;
	const btScalar Gravity = 19;

	const float InertiaFactor = 0.5f;

	const float UprightTorque = 250 * InertiaFactor;
	const float UprightTorqueMax = 125 * InertiaFactor;

	const double TurnForwardVelocityThreshold = 2;
	const float TurnForwardTorque = 100 * InertiaFactor;
	const float TurnForwardTorqueMax = 30 * InertiaFactor;
}

Sheep::Sheep(int keyUp, int keyDown, int keyLeft, int keyRight, int playerNumber, glm::mat4 initialTransform, glm::vec3 scale)
: PhysicsGameObject(scale)
, _playerNumber(playerNumber)
{
	_keyUp = keyUp;  _keyDown = keyDown; _keyLeft = keyLeft; _keyRight = keyRight;

	_modelMatrix = initialTransform;
	Material3D* newMaterial = new Material3D(
		"resources\\models\\sheep_body.obj",
		"resources\\shaders\\VolumeRendering.vertexshader",
		"resources\\shaders\\VolumeRendering.fragmentshader");
	_wool = std::make_shared<Texture3D>(playerNumber);
	newMaterial->setTexture3D(_wool);
	_material = newMaterial;

	btTransform tempTransform;
	tempTransform.setFromOpenGLMatrix(glm::value_ptr(initialTransform));
	float t = 0.2f;
	float sphereRadius = std::max(_scale.x+t, std::max(_scale.y+t, _scale.z+t));
	_rigidBody->setCollisionShape(new btSphereShape(sphereRadius));
	_rigidBody->setWorldTransform(tempTransform);

	_rigidBody->setActivationState(DISABLE_DEACTIVATION);
	_rigidBody->setMassProps(100, btVector3(InertiaFactor, InertiaFactor, InertiaFactor));
	_rigidBody->setDamping(LinearDamping, 0.9f);
	_rigidBody->setGravity(btVector3(0, -Gravity, 0));
	_rigidBody->setFriction(0.01f);

	// for continuous collision detection
	//_rigidBody->setCcdMotionThreshold(sphereRadius / 60); 
	//_rigidBody->setCcdSweptSphereRadius(sphereRadius);

	_myCamera = new FollowerCamera(this);
	SheepEngineMain::addGameObject(_myCamera);

	_sheepCore = new GameObject();
	Material* material = new Material("resources\\models\\sheep_body.obj",
		"resources\\textures\\Fur0011_1_M.bmp",
		"resources\\shaders\\ShadowMapping.vertexshader",
		"resources\\shaders\\ShadowMapping.fragmentshader");
	_sheepCore->setMaterial(material);
	addChild(_sheepCore);

	// legs
	_legfr = new Leg(vec3(-1, 0, 0));
	_legfr->translate(0.19f, 0, 0.317f);

	_legfl = new Leg(vec3(-1, 0, 0));
	_legfl->translate(-0.19f, 0, 0.317f);

	_legbr = new Leg(vec3(1, 0, 0));
	_legbr->translate(0.19f, 0, -0.317f);

	_legbl = new Leg(vec3(1, 0, 0));
	_legbl->translate(-0.19f, 0, -0.317f);

	addChild(_legfr);
	addChild(_legfl);
	addChild(_legbr);
	addChild(_legbl);
}

void Sheep::update(double dt)
{
	PhysicsGameObject::update(dt);

	/*cout << "Position: " << _modelMatrix[3][0] << ", "
	<< _modelMatrix[3][1] << ", "
	<< _modelMatrix[3][2] << endl;*/

	float deltaT = static_cast<float>(dt);

	updateOrientation(deltaT);
	updateMovement(deltaT);
	checkForParty();
}

void Sheep::updateOrientation(float dt)
{
	vec3 up(0, 1, 0);
	vec3 torque(0, 0, 0);

	// upright torque
	{
		vec3 localUp = mat3(_modelMatrix) * vec3(0, 1, 0);
		localUp = glm::normalize(localUp);

		// dependent on deviation
		float uprightTorqueFactor = UprightTorque * (1.5f - glm::dot(localUp, up)) * 0.5f;
		vec3 uprightTorque = glm::cross(localUp, up) * uprightTorqueFactor;
		double torqueLength = glm::length(uprightTorque);
		if (torqueLength > UprightTorqueMax)
		{
			uprightTorque = glm::normalize(uprightTorque) * UprightTorqueMax;
		}

		torque += uprightTorque;
	}

	// turn forward to velocity dir
	{
		btVector3 btVelocity = _rigidBody->getLinearVelocity();
		double velLength = btVector3(btVelocity.x(), 0, btVelocity.z()).length();
		if (velLength > TurnForwardVelocityThreshold)
		{
			vec3 velocity(btVelocity.x(), btVelocity.y(), btVelocity.z());
			velocity = glm::normalize(velocity);
			vec3 referenceForward = velocity;

			vec3 localForward = mat3(_modelMatrix) * vec3(0, 0, 1);
			localForward = glm::normalize(localForward);

			// dependent on deviation
			float forwardTorqueFactor = TurnForwardTorque * (-glm::dot(localForward, velocity) + 1) * 0.5f;
			vec3 forwardTorque = glm::cross(localForward, velocity) * forwardTorqueFactor;
			double torqueLength = glm::length(forwardTorque);
			if (torqueLength > TurnForwardTorqueMax)
			{
				forwardTorque = glm::normalize(forwardTorque) * TurnForwardTorqueMax;
			}

			torque += forwardTorque;
		}
	}
	_rigidBody->applyTorqueImpulse(btVector3(torque.x, torque.y, torque.z) * dt);
}

void Sheep::updateMovement(float dt)
{
	GLFWwindow* window = SheepEngineMain::getWindow();
	vec3 up = vec3(0, 1, 0);

	vec3 camToSheep = vec3(getPosition() - _myCamera->getPosition());
	camToSheep = glm::normalize(camToSheep);
	vec3 right = glm::cross(up, camToSheep);

	camToSheep.y = 0;
	camToSheep = glm::normalize(camToSheep);

	right.y = 0;
	right = glm::normalize(right);

	float duckForceFactor = _isDuck ? 1.25f : 1.0f;

	float forwardAxis = 0;
	float steerAxis = 0;

	// joystick if existing
	int numAxes = -1;
	const float* joystickAxes = glfwGetJoystickAxes(GLFW_JOYSTICK_1 + _playerNumber, &numAxes);
	if (numAxes > 0)
	{
		steerAxis = -joystickAxes[0];
	}
	if (numAxes > 2)
	{
		forwardAxis = -joystickAxes[2];
	}

	/*for (int i = 0; i < numAxes; ++i)
	{
		cout << "axis " << i << ": " << joystickAxes[i] << endl;
	}*/

	// overwrite by keyboard
	if (glfwGetKey(window, _keyUp) == GLFW_PRESS)
	{
		forwardAxis = 1;
	}
	if (glfwGetKey(window, _keyDown) == GLFW_PRESS)
	{
		forwardAxis = -1;
	}
	if (glfwGetKey(window, _keyLeft) == GLFW_PRESS)
	{
		steerAxis = 1;
	}
	if (glfwGetKey(window, _keyRight) == GLFW_PRESS)
	{
		steerAxis = -1;
	}

	_rigidBody->applyCentralImpulse(
		btVector3(camToSheep.x, camToSheep.y, camToSheep.z)
		* AccelerationImpulse * duckForceFactor * forwardAxis * dt);

	_rigidBody->applyCentralImpulse(
		btVector3(right.x, right.y, right.z)
		* AccelerationImpulse * duckForceFactor * steerAxis * dt);
}

Sheep::~Sheep()
{
}

void Sheep::setModelMatrix(const mat4 &newMatrix)
{
	_modelMatrix = newMatrix;
}

void Sheep::checkForParty()
{
	DRGameManager* gameManager = static_cast<DRGameManager*>(SheepEngineMain::getGameManager());
	if (gameManager)
	{
		GameObject* partyHat = gameManager->getPartyHat();
		if (partyHat)
		{
			if (partyHat->isVisible() && glm::length(getPosition() - partyHat->getPosition()) < 2)
			{
				_isDuck = true;
				partyHat->setVisibility(false);
				setMaterial(gameManager->getDuckMaterial());

				_sheepCore->setVisibility(false);
				_legfr->setVisibility(false);
				_legfl->setVisibility(false);
				_legbr->setVisibility(false);
				_legbl->setVisibility(false);
			}
		}
	}
}