#include "StrawBale.hpp"

#include "SheepEngineMain.hpp"
#include "Material.hpp"

#include <algorithm>    // std::max
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>

using namespace std;
using namespace glm;
using namespace SheepEngine;

namespace
{
	//const btScalar AccelerationImpulse = 1250;
	const btScalar LinearDamping = 0.5f;
	const btScalar Gravity = 5;

	const float InertiaFactor = 0.1f;
}

StrawBale::StrawBale(glm::mat4 initialTransform, glm::vec3 scale)
: PhysicsGameObject(scale)
{
	_modelMatrix = initialTransform;
	
	Material *newMaterial = new Material("resources\\models\\strawBale.obj",
		"resources\\textures\\mix_texture.bmp",
		"resources\\shaders\\ShadowMapping.vertexshader",
		"resources\\shaders\\ShadowMapping.fragmentshader");
	_material = newMaterial;

	btTransform tempTransform;
	tempTransform.setFromOpenGLMatrix(glm::value_ptr(initialTransform));

	// Box Collision Shape
	vec3 max = ResourceManager::getModelData("resources\\models\\strawBale.obj")->_maxBB;
	vec3 min = ResourceManager::getModelData("resources\\models\\strawBale.obj")->_minBB;
	vec3 size = (max - min) / 2.0f;
	_rigidBody->setCollisionShape(new btBoxShape(btVector3(size.x, size.y, size.z)));

	_rigidBody->setWorldTransform(tempTransform);

	_rigidBody->setMassProps(30, btVector3(InertiaFactor, InertiaFactor, InertiaFactor));
	_rigidBody->setDamping(LinearDamping, 0.9f);
	_rigidBody->setGravity(btVector3(0, -Gravity, 0));
	_rigidBody->setFriction(0.1f);
	_rigidBody->setAngularFactor(btVector3(0.5f, 0.5f, 0.5f));
}

StrawBale::~StrawBale()
{
}
