#include "GameObject.hpp"

#include "SheepEngineMain.hpp"
#include "CameraManager.hpp"
#include "Texture.hpp"
#include "ModelLoader.hpp"
#include "VboIndexer.hpp"

using namespace std;
using namespace glm;
using namespace SheepEngine;

GameObject::GameObject()
: _modelMatrix(1.0)
{
}

void GameObject::setMaterial(IMaterial* material)
{
	if (_material)
	{
		delete _material;
	}
	_material = material;
}

void GameObject::setParent(GameObject* parent) {
	_parent = parent;
}

const GameObject* GameObject::getParent() {
	return _parent;
}

void GameObject::addChild(GameObject* child) {
	if (child->getParent() != 0)
		cout << "child can only have one parent!";

	child->setParent(this);
	_children.push_back(child);
}

void GameObject::translate(float x, float y, float z)
{
	_modelMatrix = glm::translate(_modelMatrix, glm::vec3(x, y, z));
}

void GameObject::rotate(float x, float y, float z, float angle)
{
	_modelMatrix = glm::rotate<float>(_modelMatrix, angle, glm::vec3(x, y, z));
}

void GameObject::scale(float x, float y, float z)
{
	_modelMatrix = glm::scale(_modelMatrix, glm::vec3(x, y, z));
}

void GameObject::animateRotate(float x, float y, float z, float angle)
{
	_rotVec = glm::vec4(x, y, z, angle);
	_animationRot = true;
}

void GameObject::animateTranslate(float x, float y, float z)
{
	_transMatrix = glm::translate(glm::mat4(1.f), glm::vec3(x, y, z));
	_animationTrans = true;
}

bool GameObject::isAnimated(){
	return _animationRot || _animationTrans;
}

glm::vec4 GameObject::getRotate(){
	return _rotVec;
}

void GameObject::setAnimatetReverse(float angle)
{
	_animateAngle[0] = angle;
	_animateAngle[1] = angle;
	_animateDirection[0] = true;
	_animateDirection[1] = true;
}

void GameObject::update(double dt)
{
	if (_animationRot == true)
	{
		if (_animateDirection[0])
		{
			if (_animateDirection[1])
			{
				_modelMatrix = glm::rotate<float>(_modelMatrix, _rotVec[3] * (float)dt, vec3(_rotVec));
				_animateAngle[1] -= _rotVec[3] * (float)dt;
				
			}
			else
			{
				_modelMatrix = glm::rotate<float>(_modelMatrix, (-1) * _rotVec[3] * (float)dt, vec3(_rotVec));
				_animateAngle[1] += _rotVec[3] * (float)dt;
			}

			if (_animateAngle[1] <= 0.0f)  _animateDirection[1] = false;
			if (_animateAngle[1] >= _animateAngle[0]) _animateDirection[1] = true;
		}
		else
		{
			_modelMatrix = glm::rotate<float>(_modelMatrix, _rotVec[3] * (float)dt, vec3(_rotVec));
		}
	}

	if (_animationTrans == true)
	{
		_modelMatrix = _modelMatrix * (_transMatrix /* * (float)dt*/);
	}

	// update children
	if (!_children.empty())
	{
		//cout << "update children!" << endl;
		for (GameObject* &child : _children) {
			child->update(dt);
		}
	}
}

void GameObject::render(int playerCam)
{
	// render children first
	if (!_children.empty())
	{
		//cout << "render children!" << endl;
		for (GameObject* &child : _children) {
			child->render(playerCam);
		}
	}

	if (_material && _isVisible)
	{
		CameraInfo* camInfo = CameraManager::getCameraInfo(playerCam);
		if (camInfo)
		{
			camInfo->_renderBuffer->activate();

			glm::mat4 renderModelMatrix = getWorldspaceMatrix();
			glm::mat4 projectionMatrix = camInfo->_projectionMatrix;
			glm::mat4 viewMatrix = camInfo->_viewMatrix;

			_material->render(playerCam, renderModelMatrix, viewMatrix, projectionMatrix);

			camInfo->_renderBuffer->deactivate();
		}
	}
}

void GameObject::renderZ()
{
	// render children
	if (!_children.empty())
	{
		//cout << "render children!" << endl;
		for (GameObject* &child : _children) {
			child->renderZ();
		}
	}

	if (_material && _isVisible)
	{
		glm::mat4 renderModelMatrix = getWorldspaceMatrix();
		glm::mat4 lightViewMatrix = SheepEngineMain::getLightViewMatrix();
		glm::mat4 lightProjectionMatrix = SheepEngineMain::getLightProjectionMatrix();

		_material->renderZ(renderModelMatrix, lightViewMatrix, lightProjectionMatrix);
	}
}

GameObject::~GameObject()
{
	delete _material;
}

const glm::mat4& GameObject::getModelMatrix() const
{
	return _modelMatrix;
}

const glm::mat4 GameObject::getWorldspaceMatrix() const
{
	glm::mat4 renderModelMatrix = _modelMatrix;
	if (_parent)
	{
		renderModelMatrix = _parent->getModelMatrix() * renderModelMatrix;
	}
	return renderModelMatrix;
}

void GameObject::setModelMatrix(const mat4 &newMatrix)
{
	_modelMatrix = newMatrix;
}

glm::vec3 GameObject::getPosition()
{ 
	return vec3(getWorldspaceMatrix()[3]);
}

void GameObject::setPosition(glm::vec3 position)
{
	_modelMatrix[3][0] = position.x;
	_modelMatrix[3][1] = position.y;
	_modelMatrix[3][2] = position.z;
}

void GameObject::switchSamplingMethod(int newMinSamplingMethod, int newMaxSamplingMethod)
{
	if (_material)
	{
		_material->switchSamplingMethod(newMinSamplingMethod, newMaxSamplingMethod);
	}

	for (GameObject* &child : _children) {
		child->switchSamplingMethod(newMinSamplingMethod, newMaxSamplingMethod);
	}
}