#include "Barn.hpp"

#include "SheepEngineMain.hpp"
#include "Material.hpp"

#include <algorithm>    // std::max
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>

using namespace std;
using namespace glm;
using namespace SheepEngine;

Barn::Barn(glm::mat4 initialTransform, glm::vec3 scale)
: PhysicsGameObject(scale)
{
	Material *newMaterial = new Material("resources\\models\\barn.obj",
		"resources\\textures\\barn_textur.bmp",
		"resources\\shaders\\ShadowMapping.vertexshader",
		"resources\\shaders\\ShadowMapping.fragmentshader");
	_material = newMaterial;

	btTransform tempTransform;
	tempTransform.setFromOpenGLMatrix(glm::value_ptr(initialTransform));
	_rigidBody->setWorldTransform(tempTransform);

	_rigidBody->setLinearFactor(btVector3(0, 0, 0)); // should never move
	_rigidBody->setAngularFactor(btVector3(0, 0, 0)); // should never rotate

	// Triangle Collision Shape
	btTriangleMesh *mTriMesh = newMaterial->getTriangleMesh();

	btCollisionShape *mTriMeshShape = new btBvhTriangleMeshShape(mTriMesh, true);
	_rigidBody->setCollisionShape(mTriMeshShape);

	_rigidBody->setMassProps(1, btVector3(1, 1, 1));
}

Barn::~Barn()
{
}
