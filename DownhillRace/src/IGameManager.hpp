#pragma once

class btDynamicsWorld;

namespace SheepEngine
{
	class IGameManager
	{
	public:
		virtual void init() = 0;
		virtual void deinit() = 0;

		virtual void update(double dt) = 0;
		virtual void render() = 0;

		virtual void bulletSubStep(btDynamicsWorld *world, float timeStep) = 0;
	};
}

