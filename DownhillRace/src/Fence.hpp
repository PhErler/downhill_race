#pragma once

#include "PhysicsGameObject.hpp"

#include <glm/glm.hpp>

namespace SheepEngine
{

	class Fence : public PhysicsGameObject
	{
	public:
		Fence(glm::mat4 initialTransform = glm::mat4(), glm::vec3 scale = glm::vec3(1, 1, 1));
		virtual ~Fence();
	};
}

