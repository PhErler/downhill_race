// basing on the opengl-tutorial from
// http://www.opengl-tutorial.org/

#include "Shader.hpp"

#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
using namespace std;

#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>

using namespace SheepEngine;

Shader::Shader(std::string vertexShaderPath, std::string fragmentShaderPath, std::string geometryShaderPath)
{
	// Create and compile our GLSL program from the shaders
	_resourceID = Shader::LoadShaders(vertexShaderPath, fragmentShaderPath, geometryShaderPath);
}

Shader::~Shader()
{
	glDeleteProgram(_resourceID);
}

void Shader::activate()
{
	glUseProgram(_resourceID);
}

void Shader::deactivate()
{
}

string Shader::readFile(const string& filePath)
{
	std::string shaderCode;
	std::ifstream shaderStream(filePath, std::ios::in);
	if (shaderStream.is_open()){
		std::string Line = "";
		while (getline(shaderStream, Line))
			shaderCode += "\n" + Line;
		shaderStream.close();
		return shaderCode;
	}
	else{
		printf("Impossible to open %s. Are you in the right directory?\n", filePath);
		getchar();
		return "";
	}
}

GLuint Shader::LoadShaders(const string& vertex_file_path, const string& fragment_file_path,
	const string& geometryFilePath, GLenum gsInput, GLenum gsOutput)
{

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Link the program
	printf("Linking program\n");
	GLuint ProgramID = glCreateProgram();

	// Create the shaders
	if (!vertex_file_path.empty())
	{
		GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
		// Read the Vertex Shader code from the file
		std::string VertexShaderCode = readFile(vertex_file_path);

		// Compile Vertex Shader
		printf("Compiling shader : %s\n", vertex_file_path.c_str());
		char const * VertexSourcePointer = VertexShaderCode.c_str();
		glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
		glCompileShader(VertexShaderID);

		// Check Vertex Shader
		glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
		glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		if (InfoLogLength > 0){
			std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
			glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
			printf("%s\n", &VertexShaderErrorMessage[0]);
		}

		glAttachShader(ProgramID, VertexShaderID);
	}

	if (!fragment_file_path.empty())
	{
		GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
		// Read the Fragment Shader code from the file
		std::string FragmentShaderCode = readFile(fragment_file_path);

		// Compile Fragment Shader
		printf("Compiling shader : %s\n", fragment_file_path.c_str());
		char const * FragmentSourcePointer = FragmentShaderCode.c_str();
		glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
		glCompileShader(FragmentShaderID);

		// Check Fragment Shader
		glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
		glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		if (InfoLogLength > 0){
			std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
			glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
			printf("%s\n", &FragmentShaderErrorMessage[0]);
		}

		glAttachShader(ProgramID, FragmentShaderID);
	}

	if (!geometryFilePath.empty())
	{
		printf("Compiling shader : %s\n", geometryFilePath.c_str());
		string geometryShaderCode = readFile(geometryFilePath);
		char const * geometrySourcePointer = geometryShaderCode.c_str();

		GLuint geomShaderID = glCreateShader(GL_GEOMETRY_SHADER_EXT);

		glShaderSource(geomShaderID, 1, &geometrySourcePointer, 0);
		glCompileShader(geomShaderID);
		glGetShaderiv(geomShaderID, GL_COMPILE_STATUS, &Result);
		glGetShaderiv(geomShaderID, GL_COMPILE_STATUS, (GLint *)&InfoLogLength);

		if (InfoLogLength > 0){
			std::vector<char> geometryShaderErrorMessage(InfoLogLength + 1);
			glGetShaderInfoLog(geomShaderID, InfoLogLength, NULL, &geometryShaderErrorMessage[0]);
			printf("%s\n", &geometryShaderErrorMessage[0]);
		}

		glAttachShader(ProgramID, geomShaderID);

		glProgramParameteriEXT(ProgramID, GL_GEOMETRY_INPUT_TYPE_EXT, gsInput);
		glProgramParameteriEXT(ProgramID, GL_GEOMETRY_OUTPUT_TYPE_EXT, gsOutput);
		glProgramParameteriEXT(ProgramID, GL_GEOMETRY_VERTICES_OUT_EXT, 4);
	}

	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> ProgramErrorMessage(InfoLogLength+1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}

	//glDeleteShader(VertexShaderID);
	//glDeleteShader(FragmentShaderID);

	return ProgramID;
}
