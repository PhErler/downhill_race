#pragma once

#include "GameObject.hpp"

namespace SheepEngine
{
	class Camera : public GameObject
	{
	public:
		Camera();
		virtual ~Camera();

		virtual void update(double dt);
		virtual void render();

	protected:
		int _cameraIndex;
		glm::vec3 _position;

		// Initial horizontal angle : toward -Z
		float _horizontalAngle = 3.14f;
		// Initial vertical angle : none
		float _verticalAngle = 0.0f;
	};
}

