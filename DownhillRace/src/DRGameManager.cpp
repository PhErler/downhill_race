#include "DRGameManager.hpp"

#include "SheepEngineMain.hpp"
#include "GameObject.hpp"
#include "Camera.hpp"
#include "FollowerCamera.hpp"
#include "PhysicsGameObject.hpp"
#include "Material.hpp"
#include "Sheep.hpp"
#include "Ground.hpp"
#include "Strawbale.hpp"
#include "Fence.hpp"
#include "Stone.hpp"
#include "Scarecrow.hpp"
#include "Barn.hpp"
#include "Flag.hpp"
#include "Tree.hpp"
#include "Barrel.hpp"
#include "ParticleEmitter.hpp"
#include "Material3D.hpp"
#include "SkyboxMaterial.hpp"
#include "Material.hpp"
#include "CameraManager.hpp"

#include <BulletCollision\CollisionDispatch\btGhostObject.h>

#include <sstream>
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace std;
using namespace glm;
using namespace SheepEngine;

bool DRGameManager::frameTime = false;

namespace
{
	const float Tolerance = 1.5f;
}

DRGameManager::DRGameManager()
: _finishedPlayers(0)
, _start(nullptr)
, _finish(nullptr)
{

}

DRGameManager::~DRGameManager()
{

}

void DRGameManager::init()
{
	SheepEngineMain::setLightDir(glm::vec3(0, -1, -1));
	SheepEngineMain::setLightColor(glm::vec3(0.8f, 0.6f, 0.6f));
	SheepEngineMain::setAmbientColor(glm::vec3(0.1f, 0.1f, 0.1f));
	SheepEngineMain::setSpecularColor(glm::vec3(0.3f, 0.3f, 0.3f));
	// make ProjectionMatrix size fit the track size per texture view in gDEBugger
	SheepEngineMain::setLightProjectionMatrix(glm::ortho<float>(-150, 150, -150, 150, -150, 150));
	SheepEngineMain::setShadowMappingSampleDistance(5000);
	
	for (int i = 0; i < SheepEngineMain::getWindowParams()._numPlayers; ++i)
	{
		_finishTime.push_back(0.0f);
		_places.push_back(-1);
	}
	_placeTexture.push_back(ResourceManager::getTexture("resources\\textures\\1.bmp"));
	_placeTexture.push_back(ResourceManager::getTexture("resources\\textures\\2.bmp"));
	_placeTexture.push_back(ResourceManager::getTexture("resources\\textures\\3.bmp"));
	_placeTexture.push_back(ResourceManager::getTexture("resources\\textures\\4.bmp"));

	float leftNDC = -1.0f;
	float rightNDC = 1.f;
	float topNDC = 1.0f;
	float bottomNDC = -1.0f;

	vector<vec3> vertices; // plane made of 2 triangles
	vertices.push_back(vec3(leftNDC, bottomNDC, 0.0f));
	vertices.push_back(vec3(rightNDC, bottomNDC, 0.0f));
	vertices.push_back(vec3(leftNDC, topNDC, 0.0f));
	vertices.push_back(vec3(leftNDC, topNDC, 0.0f));
	vertices.push_back(vec3(rightNDC, bottomNDC, 0.0f));
	vertices.push_back(vec3(rightNDC, topNDC, 0.0f));
	_quadVertexBuffer = new VertexBuffer(vertices);

	_passthroughShader = ResourceManager::getShader(
		"resources\\shaders\\Passthrough.vertexshader",
		"resources\\shaders\\Passthrough.fragmentshader");

	_renderTextureID = glGetUniformLocation(_passthroughShader->getResourceID(), "eyeTexture");
	
	Ground* ground = new Ground();
	SheepEngineMain::addGameObject(ground);
	
	_start = new Flag(glm::rotate(glm::translate(mat4(), vec3(103.698, -0.895, 86.447)), 90.0f, vec3(0, 1, 0)), vec3(1.5f, 1, 1));
	SheepEngineMain::addGameObject(_start);

	_finish = new Flag(glm::rotate(glm::translate(mat4(), vec3(103.698, -14.815, 28.013)), 90.0f, vec3(0, 1, 0)), vec3(1.5f, 1, 1));
	SheepEngineMain::addGameObject(_finish);

	//Fence* zaun = new Fence(glm::translate(mat4(), vec3(8, 0, 2)));
	//SheepEngineMain::addGameObject(zaun);

	// trees
	Tree* tree1 = new Tree(1, glm::translate(mat4(), vec3(79.893, -1.315, 81.71)));
	SheepEngineMain::addGameObject(tree1);

	Tree* tree2 = new Tree(1, glm::rotate(glm::translate(mat4(), vec3(64.108, -1.14, 95.543)), -11.241f, vec3(1, 0, 0)));
	SheepEngineMain::addGameObject(tree2);

	Tree* tree3 = new Tree(1, glm::translate(mat4(), vec3(52.6, -0.787, 76.222)));
	SheepEngineMain::addGameObject(tree3);

	/*Tree* tree4 = new Tree(1, glm::translate(mat4(), vec3(45.075, 1.292, 70.093)));
	SheepEngineMain::addGameObject(tree4);*/

	Tree* tree5 = new Tree(1, glm::translate(mat4(), vec3(40.318, -2.093, 78.683)));
	SheepEngineMain::addGameObject(tree5);

	Tree* tree6 = new Tree(1, glm::translate(mat4(), vec3(15.505, -0.622, 115.308)));
	SheepEngineMain::addGameObject(tree6);

	Tree* tree7 = new Tree(1, glm::translate(mat4(), vec3(6.901, 0.444, 117.418)));
	SheepEngineMain::addGameObject(tree7);

	/*Tree* tree8 = new Tree(1, glm::translate(mat4(), vec3(-3.783, 1.899, 117.418)));
	SheepEngineMain::addGameObject(tree8);*/

	Tree* tree9 = new Tree(1, glm::translate(mat4(), vec3(6.586, -1.152, 93.783)));
	SheepEngineMain::addGameObject(tree9);

	/*Tree* tree10 = new Tree(1, glm::translate(mat4(), vec3(6.586, 0.451, 74.63)));
	SheepEngineMain::addGameObject(tree10);*/

	Tree* tree11 = new Tree(1, glm::translate(mat4(), vec3(-12.036, 1.505, 53.047)));
	SheepEngineMain::addGameObject(tree11);

	Tree* tree12 = new Tree(1, glm::translate(mat4(), vec3(-24.479, -0.499, 81.341)));
	SheepEngineMain::addGameObject(tree12);

	Tree* tree13 = new Tree(1, glm::translate(mat4(), vec3(-88.074, -7.102, 12.503)));
	SheepEngineMain::addGameObject(tree13);

	Tree* tree14 = new Tree(1, glm::translate(mat4(), vec3(-127.285, -4.33, 14.858)));
	SheepEngineMain::addGameObject(tree14);

	/*Tree* tree15 = new Tree(1, glm::translate(mat4(), vec3(-129.801, -3.393, 6.386)));
	SheepEngineMain::addGameObject(tree15);*/

	Tree* tree16 = new Tree(1, glm::translate(mat4(), vec3(-127.285, -2.024, -9.061)));
	SheepEngineMain::addGameObject(tree16);

	Tree* tree17 = new Tree(1, glm::translate(mat4(), vec3(-111.935, -5.88, -9.061)));
	SheepEngineMain::addGameObject(tree17);

	Tree* tree18 = new Tree(1, glm::translate(mat4(), vec3(-21.576, -10.33, -112.528)));
	SheepEngineMain::addGameObject(tree18);

	Tree* tree19 = new Tree(1, glm::translate(mat4(), vec3(32.699, -10.879, -105.549)));
	SheepEngineMain::addGameObject(tree19);

	Tree* tree20 = new Tree(1, glm::translate(mat4(), vec3(54.448, -9.222, -67.932)));
	SheepEngineMain::addGameObject(tree20);

	Tree* tree21 = new Tree(1, glm::translate(mat4(), vec3(59.801, -4.912, -57.473)));
	SheepEngineMain::addGameObject(tree21);

	Tree* tree22 = new Tree(1, glm::translate(mat4(), vec3(93.055, -10.842, -31.369)));
	SheepEngineMain::addGameObject(tree22);

	Tree* tree23 = new Tree(1, glm::translate(mat4(), vec3(-91.023, -6.816, -18.805)));
	SheepEngineMain::addGameObject(tree23);

	Tree* tree24 = new Tree(1, glm::translate(mat4(), vec3(-73.538, -4.378, -37.36)));
	SheepEngineMain::addGameObject(tree24);

	Tree* tree25 = new Tree(1, glm::translate(mat4(), vec3(-33.956, -8.034, 14.016)));
	SheepEngineMain::addGameObject(tree25);

	Tree* tree26 = new Tree(1, glm::translate(mat4(), vec3(38.881, -11.756, 30.883)));
	SheepEngineMain::addGameObject(tree26);

	Tree* tree27 = new Tree(1, glm::translate(mat4(), vec3(-34.868, -3.158, 55.714)));
	SheepEngineMain::addGameObject(tree27);

	Tree* tree28 = new Tree(1, glm::translate(mat4(), vec3(32.323, -12.878, -35.48)));
	SheepEngineMain::addGameObject(tree28);

	Tree* tree29 = new Tree(1, glm::translate(mat4(), vec3(-22.572, -9.272, -24.577)));
	SheepEngineMain::addGameObject(tree29);


	// treeStumps, tree type 2

	Tree* treeStump1 = new Tree(2, glm::rotate(glm::translate(mat4(), vec3(-76.031, -14.759, -72.981)), -9.86f, vec3(0, 0, 1)));
	SheepEngineMain::addGameObject(treeStump1);

	Tree* treeStump2 = new Tree(2, glm::translate(mat4(), vec3(-36.062, -16.5, -109.105)));
	SheepEngineMain::addGameObject(treeStump2);

	Tree* treeStump3 = new Tree(2, glm::translate(mat4(), vec3(4.024, -15.656, -96.481)));
	SheepEngineMain::addGameObject(treeStump3);

	Tree* treeStump4 = new Tree(2, glm::translate(mat4(), vec3(38.951, -13.933, -76.51)));
	SheepEngineMain::addGameObject(treeStump4);

	Tree* treeStump5 = new Tree(2, glm::translate(mat4(), vec3(-66.5, -14.832, -111.558)));
	SheepEngineMain::addGameObject(treeStump5);

	// treeTrunkss, tree type 3

	Tree* treeTrunk1 = new Tree(3, glm::rotate(glm::translate(mat4(), vec3(-71.561, -15.203, -102.656)), -34.692f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(treeTrunk1);

	Tree* treeTrunk2 = new Tree(3, glm::rotate(glm::translate(mat4(), vec3(-24.512, -16.301, -96.83)), -89.151f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(treeTrunk2);

	Tree* treeTrunk3 = new Tree(3, glm::rotate(glm::translate(mat4(), vec3(-7.806, -16.359, -98.9)), -47.246f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(treeTrunk3);

	Tree* treeTrunk4 = new Tree(3, glm::rotate(glm::translate(mat4(), vec3(38.609, -14.622, -103.386)), -56.713f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(treeTrunk4);

	/*Tree* treeTrunk5 = new Tree(3, glm::rotate(glm::translate(mat4(), vec3(38.887, -13.965, -85.163)), 36.989f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(treeTrunk5);*/


	// stones, type 1
	Stone* stone1_1 = new Stone(1, glm::rotate(glm::translate(mat4(), vec3(25.147, -7.843, 91.857)), 71.09f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(stone1_1);

	Stone* stone1_2 = new Stone(1, glm::rotate(glm::translate(mat4(), vec3(-11.502, -7.843, 63.701)), 71.09f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(stone1_2);

	Stone* stone1_3 = new Stone(1, glm::rotate(glm::translate(mat4(), vec3(51.444, -16.228, -27.227)), 7.05f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(stone1_3);

	Stone* stone1_4 = new Stone(1, glm::rotate(glm::translate(mat4(), vec3(-1.508, -15.254, -45.333)), 230.328f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(stone1_4);

	Stone* stone1_5 = new Stone(1, glm::rotate(glm::translate(mat4(), vec3(-76.549, -12.195, -60.071)), 230.328f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(stone1_5);

	Stone* stone1_6 = new Stone(1, glm::rotate(glm::translate(mat4(), vec3(-10.926, -8.097, 91.857)), 71.09f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(stone1_6);

	Stone* stone1_7 = new Stone(1, glm::rotate(glm::translate(mat4(), vec3(95.679, -14.725, -55.216)), 65.687f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(stone1_7);

	Stone* stone1_8 = new Stone(1, glm::rotate(glm::translate(mat4(), vec3(-1.508, -14.416, 25.585)), 290.173f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(stone1_8);


	// stones, type 2
	Stone* stone2_1 = new Stone(2, glm::rotate(glm::translate(mat4(), vec3(8.782, -7.454, 102.814)), 90.0f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(stone2_1);

	Stone* stone2_2 = new Stone(2, glm::rotate(glm::translate(mat4(), vec3(-54.755, -13.23, -120.024)), 90.0f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(stone2_2);

	Stone* stone2_3 = new Stone(2, glm::rotate(glm::translate(mat4(), vec3(79.452, -12.443, -62.292)), 240.763f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(stone2_3);

	Stone* stone2_4 = new Stone(2, glm::rotate(glm::translate(mat4(), vec3(-24.531, -12, -6.361)), 365.061f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(stone2_4);


	// stones, type 3
	Stone* stone3_1 = new Stone(3, glm::translate(mat4(), vec3(44.204, -6.444, 93.181)));
	SheepEngineMain::addGameObject(stone3_1);

	Stone* stone3_2 = new Stone(3, glm::translate(mat4(), vec3(-94.032, -6.444, 70.224)), vec3(2.357, 2.357, 2.357));
	SheepEngineMain::addGameObject(stone3_2);

	Stone* stone3_3 = new Stone(3, glm::translate(mat4(), vec3(87.533, -13.791, -42.749)));
	SheepEngineMain::addGameObject(stone3_3);

	Stone* stone3_4 = new Stone(3, glm::translate(mat4(), vec3(-60.697, -4.858, -31.186)));
	SheepEngineMain::addGameObject(stone3_4);

	Stone* stone3_5 = new Stone(3, glm::translate(mat4(), vec3(28.257, -14.391, 38.814)));
	SheepEngineMain::addGameObject(stone3_5);


	// scarecrows

	Scarecrow* scarecrow1 = new Scarecrow(1, glm::rotate(glm::translate(mat4(), vec3(-91.62, -10.495, -1.289)), 61.592f, vec3(0, 1, 0)));
	Scarecrow* scarecrow_ad1 = new Scarecrow(2, glm::translate(mat4(), vec3(0.0f, 1.762, 0.0f)));
	scarecrow1->addChild(scarecrow_ad1);
	SheepEngineMain::addGameObject(scarecrow1);


	Scarecrow* scarecrow2 = new Scarecrow(1, glm::rotate(glm::translate(mat4(), vec3(-79.62, -9.611, -21.952)), 57.443f, vec3(0, 1, 0)));
	Scarecrow* scarecrow_ad2 = new Scarecrow(2, glm::translate(mat4(), vec3(0.0f, 1.762, 0.0f)));
	scarecrow2->addChild(scarecrow_ad2);
	SheepEngineMain::addGameObject(scarecrow2);

	Scarecrow* scarecrow3 = new Scarecrow(1, glm::rotate(glm::translate(mat4(), vec3(-67.33, -10.817, -45.115)), 105.828f, vec3(0, 1, 0)));
	Scarecrow* scarecrow_ad3 = new Scarecrow(2, glm::translate(mat4(), vec3(0.0f, 1.762, 0.0f)));
	scarecrow3->addChild(scarecrow_ad3);
	SheepEngineMain::addGameObject(scarecrow3);

	Scarecrow* scarecrow4 = new Scarecrow(1, glm::rotate(glm::translate(mat4(), vec3(-76.771, -15.013, -87.51)), 105.828f, vec3(0, 1, 0)));
	Scarecrow* scarecrow_ad4 = new Scarecrow(2, glm::translate(mat4(), vec3(0.0f, 1.762, 0.0f)));
	scarecrow4->addChild(scarecrow_ad4);
	SheepEngineMain::addGameObject(scarecrow4);

	// barn area

	Barn* barn = new Barn(glm::translate(mat4(), vec3(0, 0, 0)));
	SheepEngineMain::addGameObject(barn);

	StrawBale* strawBale1 = new StrawBale(glm::rotate(glm::translate(mat4(), vec3(-92.666, -12.659, 35.387)), 22.321f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(strawBale1);

	StrawBale* strawBale2 = new StrawBale(glm::rotate(glm::translate(mat4(), vec3(-90.836, -12.659, 30.031)), 22.321f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(strawBale2);

	StrawBale* strawBale3 = new StrawBale(glm::rotate(glm::translate(mat4(), vec3(-93.939, -12.659, 28.991)), 22.321f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(strawBale3);

	StrawBale* strawBale4 = new StrawBale(glm::rotate(glm::translate(mat4(), vec3(-89.699, -12.302, 52.419)), 63.875f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(strawBale4);

	/*StrawBale* strawBale5 = new StrawBale(glm::rotate(glm::translate(mat4(), vec3(-101.301, -12.947, 44.441)), 68.435f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(strawBale5);

	StrawBale* strawBale6 = new StrawBale(glm::rotate(glm::translate(mat4(), vec3(-115.404, -11.411, 35.387)), 22.321f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(strawBale6);*/

	StrawBale* strawBale7 = new StrawBale(glm::rotate(glm::translate(mat4(), vec3(-115.404, -12.389, 24.441)), 22.321f, vec3(0, 1, 0)));
	SheepEngineMain::addGameObject(strawBale7);

	Barrel* barrel1 = new Barrel(glm::translate(mat4(), vec3(-51.021, -9.102, 65.809)));
	SheepEngineMain::addGameObject(barrel1);

	Barrel* barrel2 = new Barrel(glm::translate(mat4(), vec3(-65.041, -9.188, 52.97)));
	SheepEngineMain::addGameObject(barrel2);

	Barrel* barrel3 = new Barrel(glm::translate(mat4(), vec3(-71.713, -9.369, 65.671)));
	SheepEngineMain::addGameObject(barrel3);

	Barrel* barrel4 = new Barrel(glm::translate(mat4(), vec3(-95.997, -10.99, 56.838)));
	SheepEngineMain::addGameObject(barrel4);

	/*Barrel* barrel5 = new Barrel(glm::translate(mat4(), vec3(-95.997, -11.112, 55.175)));
	SheepEngineMain::addGameObject(barrel5);

	Barrel* barrel6 = new Barrel(glm::translate(mat4(), vec3(-97.879, -11.112, 55.175)));
	SheepEngineMain::addGameObject(barrel6*/

	Barrel* barrel7 = new Barrel(glm::translate(mat4(), vec3(-101.867, -12.553, 26.732)));
	SheepEngineMain::addGameObject(barrel7);

	/*Barrel* barrel8 = new Barrel(glm::translate(mat4(), vec3(-103.667, -10.067, -6.882)));
	SheepEngineMain::addGameObject(barrel8*/

	Barrel* barrel9 = new Barrel(glm::translate(mat4(), vec3(-101.867, -10.159, -8.177)));
	SheepEngineMain::addGameObject(barrel9);

	Barrel* barrel10 = new Barrel(glm::translate(mat4(), vec3(-104.361, -12.991, 39.569)));
	SheepEngineMain::addGameObject(barrel10);

	Barrel* barrel11 = new Barrel(glm::translate(mat4(), vec3(-108.717, -12.553, 18.294)));
	SheepEngineMain::addGameObject(barrel11);

	GameObject* skybox = new GameObject();
	skybox->setMaterial(new SkyboxMaterial());
	SheepEngineMain::addGameObject(skybox);
	
	_partyHat = new GameObject();
	_partyHat->setMaterial(new Material("resources\\models\\duck.obj",
		"resources\\textures\\duck.bmp",
		"resources\\shaders\\ShadowMapping.vertexshader",
		"resources\\shaders\\ShadowMapping.fragmentshader"));
	_partyHat->translate(-93.032f, -8.444f, 64.224f);
	_partyHat->rotate(1, 0, 0, 45);
	_partyHat->animateRotate(0, 1, 0, 45);
	_partyHat->scale(0.5f, 1, 1);
	SheepEngineMain::addGameObject(_partyHat);

	_duckMaterial = new Material("resources\\models\\duck.obj",
		"resources\\textures\\duck.bmp",
		"resources\\shaders\\ShadowMapping.vertexshader",
		"resources\\shaders\\ShadowMapping.fragmentshader");

	int playerInputs[] = {
		GLFW_KEY_W, GLFW_KEY_S, GLFW_KEY_A, GLFW_KEY_D,
		GLFW_KEY_UP, GLFW_KEY_DOWN, GLFW_KEY_LEFT, GLFW_KEY_RIGHT,
		GLFW_KEY_KP_8, GLFW_KEY_KP_5, GLFW_KEY_KP_4, GLFW_KEY_KP_6,
		GLFW_KEY_I, GLFW_KEY_K, GLFW_KEY_J, GLFW_KEY_L
	};

	// transparent objects must be last
	vec3 initPos(114.833f, 4.78f, 90.655f);
	vec3 translationPerPlayer(0, 0, -1.5f);
	for (int playerNumber = 0; playerNumber < SheepEngineMain::getWindowParams()._numPlayers; ++playerNumber)
	{
		// sheeps
		Sheep* newSheep = new Sheep(playerInputs[playerNumber * 4], playerInputs[playerNumber * 4 + 1], playerInputs[playerNumber * 4 + 2], playerInputs[playerNumber * 4 + 3],
			playerNumber, glm::rotate(glm::translate(mat4(), initPos + translationPerPlayer * float(playerNumber)), 270.0f, vec3(0, 1, 0)), vec3(0.5f, 0.5f, 0.5f));
		_sheeps.push_back(newSheep);

		SheepEngineMain::addGameObject(newSheep);

		newSheep->addChild(new ParticleEmitter());
	}

}

void DRGameManager::deinit()
{

}

string samplingMethodToString(int samplingMethod)
{
	switch (samplingMethod)
	{
	case GL_NEAREST:
		return "GL_NEAREST";
	case GL_LINEAR:
		return "GL_LINEAR";
	case GL_NEAREST_MIPMAP_NEAREST:
		return "GL_NEAREST_MIPMAP_NEAREST";
	case GL_LINEAR_MIPMAP_LINEAR:
		return "GL_LINEAR_MIPMAP_LINEAR";
	case GL_LINEAR_MIPMAP_NEAREST:
		return "GL_LINEAR_MIPMAP_NEAREST";
	case GL_NEAREST_MIPMAP_LINEAR:
		return "GL_NEAREST_MIPMAP_LINEAR";
	default:
		return "unknown";
	}
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	static int minSamplingMethod = GL_LINEAR;
	static int maxSamplingMethod = GL_LINEAR;

	if (key == GLFW_KEY_F2 && action == GLFW_PRESS)
	{
		DRGameManager::frameTime = !DRGameManager::frameTime;
	}

	if (key == GLFW_KEY_F3 && action == GLFW_PRESS)
	{
		SheepEngineMain::changeWireFrameMode();
	}

	if (key == GLFW_KEY_F4 && action == GLFW_PRESS)
	{
		if (minSamplingMethod == GL_NEAREST)
		{
			minSamplingMethod = GL_LINEAR;
		}
		else if (minSamplingMethod == GL_LINEAR)
		{
			minSamplingMethod = GL_NEAREST;
		}
		SheepEngineMain::switchSamplingMethod(maxSamplingMethod, minSamplingMethod);
		cout << "Sampling Method is now (" << samplingMethodToString(minSamplingMethod) << ", " << samplingMethodToString(maxSamplingMethod) << ")" << endl;
	}

	if (key == GLFW_KEY_F5 && action == GLFW_PRESS)
	{
		if (maxSamplingMethod == GL_NEAREST || maxSamplingMethod == GL_LINEAR)
		{
			maxSamplingMethod = GL_NEAREST_MIPMAP_NEAREST;
		}
		else if (maxSamplingMethod == GL_NEAREST_MIPMAP_NEAREST)
		{
			maxSamplingMethod = GL_LINEAR_MIPMAP_LINEAR;
		}
		else if (maxSamplingMethod == GL_LINEAR_MIPMAP_LINEAR)
		{
			maxSamplingMethod = GL_LINEAR_MIPMAP_NEAREST;
		}
		else if (maxSamplingMethod == GL_LINEAR_MIPMAP_NEAREST)
		{
			maxSamplingMethod = GL_NEAREST_MIPMAP_LINEAR;
		}
		else if (maxSamplingMethod == GL_NEAREST_MIPMAP_LINEAR)
		{
			maxSamplingMethod = minSamplingMethod;
		}
		SheepEngineMain::switchSamplingMethod(maxSamplingMethod, minSamplingMethod);
		cout << "Sampling Method is now (" << samplingMethodToString(minSamplingMethod) << ", " << samplingMethodToString(maxSamplingMethod) << ")" << endl;
	}

	if (key == GLFW_KEY_F8 && action == GLFW_PRESS)
	{
		SheepEngineMain::toggleCulling();
	}

	if (key == GLFW_KEY_F9 && action == GLFW_PRESS)
	{
		ParticleEmitter::enableBlending = !ParticleEmitter::enableBlending;
		Material3D::enableBlending = !Material3D::enableBlending;
		cout << "Blending " << (ParticleEmitter::enableBlending ? "on" : "off") << endl;
	}

}

void DRGameManager::update(double dt)
{
	GLFWwindow* window = SheepEngineMain::getWindow();
	glfwSetKeyCallback(window, key_callback);

	ostringstream titleStream;
	titleStream << "Downhill Race" << " | FPS = " << SheepEngineMain::getFPS() << " | Frametime = " << (dt * 1000.0) << "ms | Rendered Vertices = " << SheepEngineMain::getRenderedVertices();
	glfwSetWindowTitle(window, titleStream.str().c_str());
}

void DRGameManager::render()
{
	int numPlayers = SheepEngineMain::getWindowParams()._numPlayers;
	for (int player = 0; player < numPlayers; ++player)
	{

		int place = _places[player];
		if (place > 0)
		{
			CameraManager::getCameraInfo(player)->_renderBuffer->activate();

			glDisable(GL_BLEND);

			_passthroughShader->activate();

			_placeTexture[place - 1]->activate(0);
			glUniform1i(_renderTextureID, 0);
			_quadVertexBuffer->activate();

			glDrawArrays(GL_TRIANGLES, 0, _quadVertexBuffer->getVertices().size());

			_placeTexture[place - 1]->deactivate(0);
			_quadVertexBuffer->deactivate();

			_passthroughShader->deactivate();

			CameraManager::getCameraInfo(player)->_renderBuffer->deactivate();
		}
	}
}

bool DRGameManager::isCollisionObjectFinish(const btCollisionObject* collObj)
{
	if (_start)
	{
		return collObj == _finish->getRigidBody();
	}
	else
	{
		return false;
	}
}

int DRGameManager::getSheepIndex(const btCollisionObject* collObj)
{
	for (unsigned int i = 0; i < _sheeps.size(); ++i)
	{
		if (collObj == _sheeps[i]->getRigidBody())
		{
			return static_cast<int>(i);
		}
	}

	return -1;
}

void DRGameManager::bulletSubStep(btDynamicsWorld *world, float timeStep)
{
	updateFinishStates(world);
}

void DRGameManager::updateFinishStates(btDynamicsWorld *world)
{
	int numManifolds = world->getDispatcher()->getNumManifolds();
	for (int i = 0; i < numManifolds; ++i)
	{
		btPersistentManifold* contactManifold = world->getDispatcher()->getManifoldByIndexInternal(i);

		int numContacts = contactManifold->getNumContacts();
		if (numContacts > 0)
		{
			const btCollisionObject* obA = static_cast<const btCollisionObject*>(contactManifold->getBody0());
			const btCollisionObject* obB = static_cast<const btCollisionObject*>(contactManifold->getBody1());

			bool hasStartFlag = isCollisionObjectFinish(obA) || isCollisionObjectFinish(obB);
			if (!hasStartFlag)
			{
				continue;
			}

			int sheepIndex = getSheepIndex(obA);
			if (sheepIndex == -1)
			{
				sheepIndex = getSheepIndex(obB);
			}
			if (sheepIndex == -1)
			{
				continue;
			}

			//cout << "hohoho collision of sheep with start flag in substep" << endl;

			if (_finishTime[sheepIndex] == 0)
			{
				++_finishedPlayers;
				_finishTime[sheepIndex] = float(glfwGetTime());
				_places[sheepIndex] = _finishedPlayers;

				cout << "### PLACE " << _finishedPlayers << "###" << endl;
				cout << "Player " << (sheepIndex + 1) << " reached the finish line in " << _finishTime[sheepIndex] << " seconds!" << endl;
			}
		}
	}
}