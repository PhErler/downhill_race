#pragma once

#include "PhysicsGameObject.hpp"

#include <glm/glm.hpp>

namespace SheepEngine
{
	class Flag : public PhysicsGameObject
	{
	public:
		Flag(glm::mat4 initialTransform = glm::mat4(), glm::vec3 scale = glm::vec3(1, 1, 1));
		virtual ~Flag();

		glm::vec3 getSize(){ return _size; }

	private:
		glm::vec3 _size;
	};
}

