#include "Leg.hpp"

#include "SheepEngineMain.hpp"
#include "Material.hpp"
#include "PhysicsGameObject.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>

using namespace std;
using namespace glm;
using namespace SheepEngine;

namespace
{
	const float LegRotationSpeed = 450; // deg per sec
}

Leg::Leg(glm::vec3 rotAxis)
: _rotAxis(rotAxis)
{
	setMaterial(new Material("resources\\models\\sheep_leg.obj",
		"resources\\textures\\leg_texture.bmp",
		"resources\\shaders\\ShadowMapping.vertexshader",
		"resources\\shaders\\ShadowMapping.fragmentshader"));
	animateRotate(rotAxis.x, rotAxis.y, rotAxis.z, LegRotationSpeed);
	setAnimatetReverse(45.0f);
}

void Leg::update(double dt)
{
	float velocityFactor = 0.5f;
	if (_parent)
	{
		const float VelThreshold = 0.5f;
		const float VelMax = 10;
		btRigidBody *parentRB = static_cast<PhysicsGameObject*>(_parent)->getRigidBody();
		float velocity = parentRB->getLinearVelocity().length();
		velocityFactor = (velocity - VelThreshold) / (VelMax - VelThreshold);
		velocityFactor = clamp<float>(velocityFactor, 0, 1);

		animateRotate(_rotAxis.x, _rotAxis.y, _rotAxis.z, LegRotationSpeed * velocityFactor);
	}

	GameObject::update(dt);
}
