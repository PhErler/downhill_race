#pragma once

#include "RenderTexture.hpp"
#include "FrameBuffer.hpp"
#include "shader.hpp"

#include <vector>
#include <memory>

#include <glm/glm.hpp>

namespace SheepEngine
{
	class Camera;

	struct CameraInfo
	{
		CameraInfo(Camera* camera,
		glm::mat4 viewMatrix,
		glm::mat4 projectionMatrix,
		int player);

		Camera* _camera;
		glm::mat4 _viewMatrix;
		glm::mat4 _projectionMatrix;

		RenderTexture* _renderColorTexture;
		RenderTexture* _renderDepthTexture;
		FrameBuffer* _renderBuffer;
		VertexBuffer* _quadVertexBuffer;
		int _player;
	};

	class CameraManager
	{
	public:
		static void init();

		static unsigned int addCamera(Camera* camera); // returns index of new camera
		static void removeCamera(unsigned int cameraIndex);
		static unsigned int getMaxCameraIndex();

		static CameraInfo* getCameraInfo(unsigned int cameraIndex);

		static void setViewMatrix(unsigned int cameraIndex, glm::mat4 newViewMatrix);
		static void setProjectionMatrix(unsigned int cameraIndex, glm::mat4 newProjectionMatrix);

		static void renderToScreen();

		static void calcBordersForNDC(int player, float &top_out, float &bottom_out, float &left_out, float &right_out);
		static void calcBordersForScreen(int player, int &left_out, int &top_out, int &width_out, int &height_out);
		static void calcRenderbufferSize(int &width_out, int &height_out);

	private:
		static std::vector<CameraInfo> s_cameraInfo;

		static std::shared_ptr<Shader> s_passthroughShader;
		static GLuint s_renderTextureID;
	};
}

