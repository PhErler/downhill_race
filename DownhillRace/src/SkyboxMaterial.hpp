#pragma once

#include "IMaterial.hpp"

#include "Shader.hpp"
#include "CubemapTexture.hpp"
#include "ResourceManager.hpp"

#include "glm\glm.hpp"
#include <GL/glew.h>
#include <memory>

namespace SheepEngine
{
	class SkyboxMaterial : public IMaterial {
	public:

		SkyboxMaterial();

		virtual void render(
			int playerNumber,
			const glm::mat4 &modelMatrix,
			const glm::mat4 &viewMatrix,
			const glm::mat4 &projectionMatrix);

		// z-buffer pass for shadow mapping
		virtual void renderZ(
			const glm::mat4 &modelMatrix,
			const glm::mat4 &lightViewMatrix,
			const glm::mat4 &lightProjectionMatrix){};

	private:
		std::shared_ptr<Shader> _program;
		std::shared_ptr<ModelData> _modelData;
		std::shared_ptr<CubemapTexture> _cubeTexture;
	};
}