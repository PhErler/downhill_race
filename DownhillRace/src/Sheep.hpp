#pragma once

#include "PhysicsGameObject.hpp"

#include "Texture3D.hpp"
#include "FollowerCamera.hpp"

#include <memory>

#include <glm/glm.hpp>

namespace SheepEngine
{
	class Camera;

	class Sheep : public PhysicsGameObject
	{
	public:
		Sheep(int keyUp, int keyDown, int keyLeft, int keyRight, int playerNumber, glm::mat4 initialTransform = glm::mat4(), glm::vec3 scale = glm::vec3(1, 1, 1));
		virtual ~Sheep();

		virtual void update(double dt);

		virtual void setModelMatrix(const glm::mat4 &newMatrix);

	private:
		void updateOrientation(float dt);
		void updateMovement(float dt);
		void checkForParty();

		int _keyDown, _keyUp, _keyLeft, _keyRight;

		Camera* _myCamera = nullptr;
		GameObject* _sheepCore = nullptr;
		GameObject* _legfr = nullptr;
		GameObject* _legfl = nullptr;
		GameObject* _legbr = nullptr;
		GameObject* _legbl = nullptr;

		std::shared_ptr<Texture3D> _wool;
		int _playerNumber = -1;

		bool _isDuck = false;
	};
}

