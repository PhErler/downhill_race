#pragma once

#include "Resource.hpp"
#include "Texture.hpp"
#include "Shader.hpp"

#include <map>
#include <string>
#include <memory>

#include "glm\glm.hpp"

namespace SheepEngine
{
	struct ModelData
	{
		ModelData(std::vector<glm::vec3>& vertexBuffer, std::vector<glm::vec2>& uvBuffer, std::vector<glm::vec3>& normalBuffer);
		~ModelData() {}

		VertexBuffer _vertexBuffer;
		UVBuffer _uvBuffer;
		NormalBuffer _normalBuffer;
		glm::vec3 _minBB;
		glm::vec3 _maxBB;
	};

	class ResourceManager
	{
	public:
		// returns the same pointer for the same inputs.
		static std::shared_ptr<Shader> getShader(std::string vertexShaderPath, std::string fragmentShaderPath, std::string geometryShaderPath = "");
		static std::shared_ptr<ModelData> getModelData(std::string modelPath);
		static std::shared_ptr<Texture> getTexture(std::string texturePath);

	private:
		static std::map<std::string, std::shared_ptr<Shader>> s_shaderMap;
		static std::map<std::string, std::shared_ptr<ModelData>> s_ModelDataMap;
		static std::map<std::string, std::shared_ptr<Texture>> s_textureMap;
	};
}

