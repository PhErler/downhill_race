#include "SheepEngineMain.hpp"

#include "Camera.hpp"
#include "CameraManager.hpp"
#include "Shader.hpp"
#include "Texture.hpp"
#include "ModelLoader.hpp"
#include "BulletDebugDrawer.hpp"
#include "ResourceManager.hpp"

#include "SDL.h"

#ifdef _DEBUG
//#define ENABLE_BULLET_DEBUG_DRAW
#endif

#ifdef ENABLE_BULLET_DEBUG_DRAW
#include "BulletDebugDrawer.hpp"
#endif

#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"

#include <string>
#include <iostream>

using namespace std;
using namespace glm;
using namespace SheepEngine;

// init static members
bool SheepEngineMain::s_initialized = false;
int SheepEngineMain::s_errorCode = EXIT_SUCCESS;
WindowParams SheepEngineMain::s_windowParams = WindowParams(1024, 768, 60, false, 1);
GLFWwindow* SheepEngineMain::s_window = nullptr;
std::vector<GameObject*> SheepEngineMain::s_gameObjects = std::vector<GameObject*>();
btBroadphaseInterface* SheepEngineMain::s_broadphase = nullptr;
btDefaultCollisionConfiguration* SheepEngineMain::s_collisionConfiguration = nullptr;
btCollisionDispatcher* SheepEngineMain::s_dispatcher = nullptr;
btSequentialImpulseConstraintSolver* SheepEngineMain::s_solver = nullptr;
btDiscreteDynamicsWorld* SheepEngineMain::s_physicsWorld = nullptr;
IGameManager* SheepEngineMain::s_gameManager = nullptr;

bool SheepEngineMain::wireFrameMode = false;
double SheepEngineMain::s_fps = 0;

bool SheepEngineMain::s_cullingEnabled = true;
int SheepEngineMain::s_verticesRenderedThisFrame = 0;

// lighting
glm::vec3 SheepEngineMain::s_LightDir = glm::vec3(0, 1, 1);
glm::vec3 SheepEngineMain::s_LightColor = glm::vec3(0.8f, 0.6f, 0.6f);
glm::vec3 SheepEngineMain::s_AmbientColor = glm::vec3(0.1f, 0.1f, 0.1f);
glm::vec3 SheepEngineMain::s_SpecularColor = glm::vec3(0.3f, 0.3f, 0.3f);

// shadow mapping
FrameBuffer* SheepEngineMain::s_shadowFrameBuffer = nullptr;
std::shared_ptr<Shader> SheepEngineMain::s_depthProgram;
GLuint SheepEngineMain::s_lightMVPID = 0;
RenderTexture* SheepEngineMain::s_shadowTexture = nullptr;
glm::mat4 SheepEngineMain::s_lightViewMatrix = glm::lookAt(glm::vec3(0, 0, 0), s_LightDir, glm::vec3(0, 1, 0));
glm::mat4 SheepEngineMain::s_lightProjectionMatrix = glm::ortho<float>(-100, 100, -650, 200, -400, 15);
float SheepEngineMain::s_shadowMappingSampleDistance = 1000;
Rect SheepEngineMain::s_currViewport(0,0,0,0);

unsigned char *SheepEngineMain::s_audioPos = 0; // global pointer to the audio buffer to be played
unsigned int SheepEngineMain::s_audioLen = 0; // remaining length of the sample we have to play
unsigned char *SheepEngineMain::s_wavBuffer; // buffer containing our audio file

namespace
{
#ifdef _DEBUG
	const int ShadowMapSize = 512;
#else
	const int ShadowMapSize = 4096; // is good enough
#endif
}

int SheepEngineMain::init(int argc, char* argv[])
{
	cout << "SheepEngine init start" << endl;

	initSound();

	if (s_initialized)
	{
		return EXIT_FAILURE;
	}

	s_initialized = true;

	s_windowParams = parseCommandLineParams(argc, argv);

	// Initialise GLFW
	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		s_errorCode = EXIT_FAILURE;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_REFRESH_RATE, s_windowParams._refreshRate);

	GLFWmonitor* monitor = NULL;
	if (s_windowParams._fullScreen)
	{
		monitor = glfwGetPrimaryMonitor();
	}

	// Open a window and create its OpenGL context
	s_window = glfwCreateWindow(s_windowParams._xRes, s_windowParams._yRes, "Downhill Race", monitor, NULL);
	if (s_window == nullptr){
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible.\n");
		glfwTerminate();
		s_errorCode = EXIT_FAILURE;
	}
	glfwMakeContextCurrent(s_window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		s_errorCode = EXIT_FAILURE;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(s_window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetCursorPos(s_window, s_windowParams._xRes / 2, s_windowParams._yRes / 2);

	// enable 3d texturing
	glEnable(GL_TEXTURE_3D);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	//glDepthFunc(GL_LESS);// default setting
	
	// Enable blending for transparency
	glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);// default setting

	// Cull back-facing triangles -> draw only front-facing triangles
	glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);// default setting

	// Draw as wireframe
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	// Necessary? our texture colors will replace the untextured colors
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	initShadowMapping();

	// init bullet stuff
	s_broadphase = new btDbvtBroadphase(); 
	s_collisionConfiguration = new btDefaultCollisionConfiguration();
	s_dispatcher = new btCollisionDispatcher(s_collisionConfiguration);
	//btGImpactCollisionAlgorithm::registerAlgorithm(s_dispatcher);
	s_solver = new btSequentialImpulseConstraintSolver;
	s_physicsWorld = new btDiscreteDynamicsWorld(s_dispatcher, s_broadphase,
		s_solver, s_collisionConfiguration);
	s_physicsWorld->setGravity(btVector3(0, -10, 0));

	s_physicsWorld->setInternalTickCallback(bulletSubStepCallback);

	s_physicsWorld->setDebugDrawer(new BulletDebugDrawer());

	CameraManager::init();

	if (s_gameManager)
	{
		s_gameManager->init();
	}

	cout << "SheepEngine init end" << endl;

	return s_errorCode;
}

int SheepEngineMain::deinit()
{
	cout << "SheepEngine deinit start" << endl;

	deinitSound();

	if (s_gameManager)
	{
		s_gameManager->deinit();
	}

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	// can be physics objects, delete before physics world
	for (GameObject* &gameObject : s_gameObjects) {
		delete gameObject;
	}

	delete s_shadowFrameBuffer;
	delete s_shadowTexture;
	s_depthProgram.reset();

	// delete bullet stuff
	delete s_physicsWorld;
	delete s_solver;
	delete s_dispatcher;
	delete s_collisionConfiguration;
	delete s_broadphase;

	cout << "SheepEngine deinit end" << endl;
	return s_errorCode;
}

int SheepEngineMain::run()
{
	double deltaT = 0.0f;
	double lastTime = 0.0f;
	static double timeCounter = 0;

	cout << "SheepEngine run start" << endl;

	// main loop
	do{
		double time = glfwGetTime();
		deltaT = time - lastTime;
		lastTime = time;

		// frames per second
		s_fps = 1 / deltaT;

		// skip update if dt is too large to avoid instable physics (can be caused by loading)
		if (deltaT < 0.25f)
		{
			// update physics
			s_physicsWorld->stepSimulation(static_cast<float>(deltaT), 10, static_cast<float>(deltaT) / 10);

			// update gamemanager
			if (s_gameManager)
			{
				s_gameManager->update(deltaT);
			}

			// first update all
			for (GameObject* &gameObject : s_gameObjects) {
				gameObject->update(deltaT);
			}
		}

		s_verticesRenderedThisFrame = 0;

		// 2 passes for shadow mapping
		zBufferPass();
		shadingPass();

		if (s_gameManager)
		{
			s_gameManager->render();
		}

		// bullet debug draw
#ifdef ENABLE_BULLET_DEBUG_DRAW
		s_physicsWorld->debugDrawWorld();
#endif

		CameraManager::renderToScreen();

		// Swap buffers
		glfwSwapBuffers(s_window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(s_window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
	glfwWindowShouldClose(s_window) == 0);

	cout << "SheepEngine run end" << endl;

	return s_errorCode;
}

GLFWwindow* SheepEngineMain::getWindow()
{
	return s_window;
}

WindowParams SheepEngineMain::parseCommandLineParams(int argc, char* argv[])
{
	int xRes = 1024;
	int yRes = 768;
	int refreshRate = 60;
	int fullScreen = 0;
	int numPlayers = 1;

	// Note: first param is our program's name
	try
	{
		if (argc >= 3)
		{
			xRes = std::stoi(string(argv[1]));
			yRes = std::stoi(string(argv[2]));
		}
		if (argc >= 4)
		{
			refreshRate = std::stoi(string(argv[3]));
		}
		if (argc >= 5)
		{
			fullScreen = std::stoi(string(argv[4]));
		}
		if (argc >= 6)
		{
			int players = std::stoi(string(argv[5]));
			if (players >= 1 && players <= 4)
			{
				numPlayers = players;
			}
		}
	}
	catch (const std::invalid_argument& ia) {
		std::cout << "Invalid argument: " << ia.what() << '\n';
	}
	catch (const std::out_of_range& oor) {
		std::cerr << "Out of Range error: " << oor.what() << '\n';
	}

	return WindowParams(xRes, yRes, refreshRate, fullScreen == 0 ? false : true, numPlayers);
}

void SheepEngineMain::bulletSubStepCallback(btDynamicsWorld *world, btScalar timeStep)
{
	if (s_gameManager)
	{
		s_gameManager->bulletSubStep(world, timeStep);
	}
}

const WindowParams& SheepEngineMain::getWindowParams()
{
	return s_windowParams;
}

void SheepEngineMain::initShadowMapping()
{
	s_shadowTexture = new RenderTexture(ShadowMapSize, ShadowMapSize,
		RenderTextureType::Depth);
	s_shadowFrameBuffer = new FrameBuffer(nullptr, s_shadowTexture);
	s_depthProgram = ResourceManager::getShader("resources\\shaders\\DepthRTT.vertexshader",
		"resources\\shaders\\DepthRTT.fragmentshader");
	s_lightMVPID = glGetUniformLocation(s_depthProgram->getResourceID(), "lightMVP");
}

void SheepEngineMain::zBufferPass()
{
	// Render to our framebuffer
	s_shadowFrameBuffer->activate();
	s_shadowFrameBuffer->clear(false, true, 0, 0, 0, 0);
	s_shadowFrameBuffer->deactivate();

	// prevent shadow acne
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(4.0f, 0.0f); // try some values

	for (GameObject* &gameObject : s_gameObjects) {
		gameObject->renderZ();
	}
}

void SheepEngineMain::shadingPass()
{
	// prevent shadow acne; reset to 0
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(0.0f, 0.0f);

	for (int playerCam = 0; playerCam < SheepEngineMain::getWindowParams()._numPlayers; ++playerCam)
	{
		CameraInfo* camInfo = CameraManager::getCameraInfo(playerCam);
		if (camInfo)
		{
			camInfo->_renderBuffer->activate();
			camInfo->_renderBuffer->clear(true, true, 0.2f, 0.6f, 0.2f, 0.0f);
			camInfo->_renderBuffer->deactivate();

			for (GameObject* &gameObject : s_gameObjects) {
				gameObject->render(playerCam);
			}
		}
	}
}

void SheepEngineMain::setLightDir(glm::vec3 lightDir) 
{ 
	s_LightDir = normalize(lightDir);
	s_lightViewMatrix = glm::lookAt(glm::vec3(0, 0, 0), s_LightDir, glm::vec3(0, 1, 0));
}

void SheepEngineMain::setLightColor(glm::vec3 lightColor) 
{ 
	s_LightColor = lightColor; 
}

void SheepEngineMain::setAmbientColor(glm::vec3 ambientColor) 
{ 
	s_AmbientColor = ambientColor; 
}

void SheepEngineMain::setSpecularColor(glm::vec3 specularColor) 
{ 
	s_SpecularColor = specularColor; 
}

void SheepEngineMain::changeWireFrameMode()
{
	wireFrameMode = !wireFrameMode;
	cout << "wireframe mode " << (SheepEngineMain::wireFrameMode ? "on" : "off") << endl;
}

void SheepEngineMain::switchSamplingMethod(int newMinSamplingMethod, int newMaxSamplingMethod)
{
	for (GameObject* &gameObject : s_gameObjects) {
		gameObject->switchSamplingMethod(newMinSamplingMethod, newMaxSamplingMethod);
	}
}

void SheepEngineMain::addRenderedVertices(int count)
{ 
	s_verticesRenderedThisFrame += count; 
}

int SheepEngineMain::getRenderedVertices()
{
	return s_verticesRenderedThisFrame;
}

void SheepEngineMain::toggleCulling()
{
	s_cullingEnabled = !s_cullingEnabled;
	cout << "culling " << (SheepEngineMain::s_cullingEnabled ? "on" : "off") << endl;
}

double SheepEngineMain::getFPS()
{
	return s_fps;
}

void SheepEngineMain::setViewPort(Rect viewPort)
{
	if (viewPort._left != s_currViewport._left ||
		viewPort._top != s_currViewport._top ||
		viewPort._width != s_currViewport._width ||
		viewPort._height != s_currViewport._height)
	{
		glViewport(viewPort._left, viewPort._top, viewPort._width, viewPort._height);
		s_currViewport = viewPort;
	}
}

// audio callback function
// here you have to copy the data of your audio buffer into the
// requesting audio buffer (stream)
// you should only copy as much as the requested length (len)
void SheepEngineMain::audioCallback(void *userdata, unsigned char *stream, int len)
{

	if (s_audioLen == 0)
		return;

	len = (len > int(s_audioLen) ? int(s_audioLen) : len);
	SDL_memcpy(stream, s_audioPos, len); 					// simply copy from one buffer into the other
	//SDL_MixAudio(stream, s_audioPos, len, SDL_MIX_MAXVOLUME);// mix from one buffer into another

	s_audioPos += len;
	s_audioLen -= unsigned int(len);
}


// thanks to https://gist.github.com/armornick/3447121
void SheepEngineMain::initSound()
{
	const string MusicPath = "resources\\sounds\\carrie_sheep_version.wav";

	// sdl audio
	int success = SDL_Init(SDL_INIT_AUDIO);
	if (success < 0)
	{
		return;
	}

	// local variables
	static Uint32 wav_length; // length of our sample
	static SDL_AudioSpec wav_spec; // the specs of our piece of music

	/* Load the WAV */
	// the specs, length and buffer of our wav are filled
	if (SDL_LoadWAV(MusicPath.c_str(), &wav_spec, &s_wavBuffer, &wav_length) == NULL){
		return;
	}
	// set the callback function
	//typedef void (SDLCALL * SDL_AudioCallback) (void *userdata, Uint8 * stream, int len);
	wav_spec.callback = audioCallback;
	wav_spec.userdata = NULL;
	// set our global static variables
	s_audioPos = s_wavBuffer; // copy sound buffer
	s_audioLen = wav_length; // copy file length

	/* Open the audio device */
	if (SDL_OpenAudio(&wav_spec, NULL) < 0){
		fprintf(stderr, "Couldn't open audio: %s\n", SDL_GetError());
		//exit(-1);
		return;
	}

	/* Start playing */
	SDL_PauseAudio(0);

	// wait until we're don't playing
	/*while (s_audioLen > 0) {
		SDL_Delay(100);
	}*/
}

void SheepEngineMain::deinitSound()
{
	// shut everything down
	SDL_CloseAudio();
	SDL_FreeWAV(s_wavBuffer);
}