#include "Texture.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <iostream>

#include <GL/glew.h>

#include <GLFW/glfw3.h>

using namespace std;
using namespace SheepEngine;

Texture::Texture(std::string path)
{
	loadImage(path);
}

void Texture::loadImage(std::string path, int newMinSamplingMethod, int newMaxSamplingMethod)
{
	string last4Chars = path.substr(path.length() - 4, string::npos);
	// to lower case
	std::transform(last4Chars.begin(), last4Chars.end(), last4Chars.begin(), ::tolower);

	if (last4Chars == ".dds")
	{
		_resourceID = Texture::loadDDS(path.c_str(), newMinSamplingMethod, newMaxSamplingMethod);
	}
	else if (last4Chars == ".bmp")
	{
		_resourceID = Texture::loadBMP_custom(path.c_str(), newMinSamplingMethod, newMaxSamplingMethod);
	}
	else
	{
		_resourceID = 0;
		cout << "WARNING: Unsupported texture format: \"" << last4Chars << "\" of file: \"" << path << "\"";
	}
}

void Texture::unloadImage()
{
	if (_resourceID)
	{
		glDeleteTextures(1, &_resourceID);
	}
}

Texture::~Texture()
{
	unloadImage();
}

void Texture::activate(unsigned int textureSampler)
{
	// Bind our texture in Texture Unit 0
	activateTextureUnit(textureSampler);
	glBindTexture(GL_TEXTURE_2D, _resourceID);
}

void Texture::deactivate(unsigned int textureSampler)
{
	//activateTextureUnit(textureSampler);
	//glBindTexture(GL_TEXTURE_2D, 0);
}

// basing on the opengl-tutorial from
// http://www.opengl-tutorial.org/
GLuint Texture::loadBMP_custom(string imagepath, int newMinSamplingMethod, int newMaxSamplingMethod)
{
	vector<char> data;
	int width = 0;
	int height = 0;

	_path = imagepath;

	loadBMP(imagepath, data, width, height);

	// Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);
	
	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, &data[0]);

	// OpenGL has now copied the data. Free our own version
	data.clear();

	// ... nice trilinear filtering.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, newMaxSamplingMethod);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, newMinSamplingMethod);
	glGenerateMipmap(GL_TEXTURE_2D);

	//glBindTexture(GL_TEXTURE_2D, 0);

	// Return the ID of the texture we just created
	return textureID;
}

#define FOURCC_DXT1 0x31545844 // Equivalent to "DXT1" in ASCII
#define FOURCC_DXT3 0x33545844 // Equivalent to "DXT3" in ASCII
#define FOURCC_DXT5 0x35545844 // Equivalent to "DXT5" in ASCII

// basing on the opengl-tutorial from
// http://www.opengl-tutorial.org/
GLuint Texture::loadDDS(const char * imagepath, int newMinSamplingMethod, int newMaxSamplingMethod){

	unsigned char header[124];

	FILE *fp;

	_path = imagepath;
 
	/* try to open the file */ 
	fp = fopen(imagepath, "rb"); 
	if (fp == NULL){
		printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", imagepath); getchar(); 
		return 0;
	}
   
	/* verify the type of file */ 
	char filecode[4]; 
	fread(filecode, 1, 4, fp); 
	if (strncmp(filecode, "DDS ", 4) != 0) { 
		fclose(fp); 
		return 0; 
	}
	
	/* get the surface desc */ 
	fread(&header, 124, 1, fp); 

	unsigned int height      = *(unsigned int*)&(header[8 ]);
	unsigned int width	     = *(unsigned int*)&(header[12]);
	unsigned int linearSize	 = *(unsigned int*)&(header[16]);
	unsigned int mipMapCount = *(unsigned int*)&(header[24]);
	unsigned int fourCC      = *(unsigned int*)&(header[80]);

 
	unsigned char * buffer;
	unsigned int bufsize;
	/* how big is it going to be including all mipmaps? */ 
	bufsize = mipMapCount > 1 ? linearSize * 2 : linearSize; 
	buffer = (unsigned char*)malloc(bufsize * sizeof(unsigned char)); 
	fread(buffer, 1, bufsize, fp); 
	/* close the file pointer */ 
	fclose(fp);

	unsigned int components  = (fourCC == FOURCC_DXT1) ? 3 : 4; 
	unsigned int format;
	switch(fourCC) 
	{ 
	case FOURCC_DXT1: 
		format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT; 
		break; 
	case FOURCC_DXT3: 
		format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT; 
		break; 
	case FOURCC_DXT5: 
		format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT; 
		break; 
	default: 
		free(buffer); 
		return 0; 
	}

	// Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);
	glPixelStorei(GL_UNPACK_ALIGNMENT,1);	
	
	unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16; 
	unsigned int offset = 0;

	/* load the mipmaps */ 
	for (unsigned int level = 0; level < mipMapCount && (width || height); ++level) 
	{ 
		unsigned int size = ((width+3)/4)*((height+3)/4)*blockSize; 
		glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height,  
			0, size, buffer + offset); 
	 
		offset += size; 
		width  /= 2; 
		height /= 2; 

		// Deal with Non-Power-Of-Two textures. This code is not included in the webpage to reduce clutter.
		if(width < 1) width = 1;
		if(height < 1) height = 1;

	} 

	free(buffer); 

	//glBindTexture(GL_TEXTURE_2D, 0);

	return textureID;
}

void Texture::switchSamplingMethod(int newMinSamplingMethod, int newMaxSamplingMethod)
{
	if (_resourceID)
	{
		glDeleteTextures(1, &_resourceID);
	}

	loadImage(_path, newMinSamplingMethod, newMaxSamplingMethod);
}