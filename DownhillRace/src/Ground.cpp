#include "Ground.hpp"

#include "Material.hpp"

#include <string>

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace std;
using namespace glm;
using namespace SheepEngine;

Ground::Ground()
: PhysicsGameObject(vec3(1,1,1))
{
	mat4 initialTransform = mat4();

	_modelMatrix = initialTransform;

	Material *newMaterial = new Material("resources\\models\\ground.obj",
		"resources\\textures\\ground_texture.bmp",
		"resources\\shaders\\ShadowMapping.vertexshader",
		"resources\\shaders\\ShadowMapping.fragmentshader");
	//newMaterial->setTextureTiling(vec2(100, 100));
	_material = newMaterial;

	btTransform tempTransform;
	tempTransform.setFromOpenGLMatrix(glm::value_ptr(initialTransform));
	_rigidBody->setWorldTransform(tempTransform);

	_rigidBody->setActivationState(DISABLE_DEACTIVATION);
	_rigidBody->setLinearFactor(btVector3(0, 0, 0)); // should never move
	_rigidBody->setAngularFactor(btVector3(0, 0, 0)); // should never rotate


	// Triangle Collision Shape
	btTriangleMesh *mTriMesh = newMaterial->getTriangleMesh();

	btCollisionShape *mTriMeshShape = new btBvhTriangleMeshShape(mTriMesh, true);
	_rigidBody->setCollisionShape(mTriMeshShape);

	_rigidBody->setMassProps(1, btVector3(1, 1, 1));
}

Ground::~Ground()
{
}
