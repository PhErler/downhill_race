#include "SkyboxMaterial.hpp"

#include "SheepEngineMain.hpp"
#include "Resource.hpp"
#include "CameraManager.hpp"

#include "glm\glm.hpp"
#include "glm\gtc\type_ptr.hpp"

using namespace std;
using namespace glm;
using namespace SheepEngine;

SkyboxMaterial::SkyboxMaterial()
{
	_program = ResourceManager::getShader(
		"resources\\shaders\\Skybox.vertexshader",
		"resources\\shaders\\Skybox.fragmentshader");

	_modelData = ResourceManager::getModelData("resources\\models\\skybox.obj");

	_cubeTexture = make_shared<CubemapTexture>("resources\\textures\\",
		"posx.bmp", "negx.bmp", "posy.bmp",
		"negy.bmp", "posz.bmp", "negz.bmp");
}

void SkyboxMaterial::render(
	int playerNumber,
	const glm::mat4 &modelMatrix,
	const glm::mat4 &viewMatrix,
	const glm::mat4 &projectionMatrix)
{
	_program->activate();

	glDisable(GL_CULL_FACE);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDepthMask(GL_FALSE);

	// Draw skybox as last
	glDepthFunc(GL_LEQUAL);  // Change depth function so depth test passes when values are equal to depth buffer's content
	mat4 view = mat4(mat3(viewMatrix));	// Remove any translation component of the view matrix
	glUniformMatrix4fv(glGetUniformLocation(_program->getResourceID(), "view"), 1, GL_FALSE, glm::value_ptr(view));
	glUniformMatrix4fv(glGetUniformLocation(_program->getResourceID(), "projection"), 1, GL_FALSE, glm::value_ptr(projectionMatrix));
	// skybox cube
	_modelData->_vertexBuffer.activate();

	_cubeTexture->activate(0);

	glDrawArrays(GL_TRIANGLES, 0, 36);

	_cubeTexture->deactivate(0);

	_modelData->_vertexBuffer.deactivate();
	glDepthFunc(GL_LESS); // Set depth function back to default

	glDepthMask(GL_TRUE);

	glEnable(GL_CULL_FACE);

	_program->deactivate();
}