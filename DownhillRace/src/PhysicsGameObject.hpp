#pragma once

#include "GameObject.hpp"

#include <btBulletDynamicsCommon.h>

#include <string>
#include <glm/glm.hpp>

namespace SheepEngine
{
	class MotionState;

	class PhysicsGameObject : public GameObject
	{
	public:
		PhysicsGameObject(glm::vec3 scale/* = glm::vec3(1,1,1)*/);
		virtual ~PhysicsGameObject();

		//virtual void update(double dt); //update is done by bullet and the motion state

		btRigidBody* getRigidBody() { return _rigidBody; }

		glm::vec3 getScale(){ return _scale; }

	protected:
		btCollisionShape* _collisionShape;
		MotionState* _motionState;
		btRigidBody* _rigidBody;

		// bullet does not save the scale in its matrix
		glm::vec3 _scale = glm::vec3(1, 1, 1);
	};
}

