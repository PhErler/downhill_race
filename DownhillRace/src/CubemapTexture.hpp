#pragma once

#include "Resource.hpp"

#include <string>

namespace SheepEngine
{
	class CubemapTexture : public ITexture
	{
	public:

		CubemapTexture(const std::string& Directory,
			const std::string& PosXFilename,
			const std::string& NegXFilename,
			const std::string& PosYFilename,
			const std::string& NegYFilename,
			const std::string& PosZFilename,
			const std::string& NegZFilename);

		~CubemapTexture();

		bool Load();

		virtual void activate(unsigned int textureSampler);
		virtual void deactivate(unsigned int textureSampler);

	private:

		std::string m_fileNames[6];
	};
}