#include "RenderTexture.hpp"

using namespace std;
using namespace SheepEngine;

RenderTexture::RenderTexture(GLuint width, GLuint height, RenderTextureType::Enum type)
{
	// render texture. Slower than a render buffer, but you can sample it later in your shader
	glGenTextures(1, &_resourceID);
	glBindTexture(GL_TEXTURE_2D, _resourceID);

	if (type == RenderTextureType::RGB)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 
			width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
	}
	else if (type == RenderTextureType::Depth)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32,
			width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);

		// works only with sampler2DShadow
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LESS);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);

	}
	else if (type == RenderTextureType::RGBA)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
			width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	}
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	// edge handling
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	_width = width;
	_height = height;

	//glBindTexture(GL_TEXTURE_2D, 0);
}

RenderTexture::~RenderTexture()
{
	glDeleteTextures(1, &_resourceID);
}

void RenderTexture::activate(unsigned int textureSampler)
{
	activateTextureUnit(textureSampler);
	glBindTexture(GL_TEXTURE_2D, _resourceID);
}

void RenderTexture::deactivate(unsigned int textureSampler)
{
	//activateTextureUnit(textureSampler);
	//glBindTexture(GL_TEXTURE_2D, 0);
}
