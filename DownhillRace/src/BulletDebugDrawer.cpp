#include "BulletDebugDrawer.hpp"

#include "CameraManager.hpp"
#include "SheepEngineMain.hpp"

#include <GL/glew.h>
#include <glm/glm.hpp>

using namespace std;
using namespace glm;
using namespace SheepEngine;

// based on: http://www.falloutsoftware.com/tutorials/gl/gl3.htm
BulletDebugDrawer::BulletDebugDrawer()
: _modelMatrix()
, _vertexBuffer(nullptr)
, _matrixID(0)
, _viewMatrixID(0)
, _modelMatrixID(0)
{
	_shader = new Shader("resources\\shaders\\Wireframe.vertexshader",
		"resources\\shaders\\Wireframe.fragmentshader");
	GLuint programID = _shader->getResourceID();

	// Get a handle for our "MVP" uniform
	_matrixID = glGetUniformLocation(programID, "MVP");
	_viewMatrixID = glGetUniformLocation(programID, "V");
	_modelMatrixID = glGetUniformLocation(programID, "M");
	
	// Load it into a VBO
	_vertexBuffer = new VertexBuffer(_vertices);
}


BulletDebugDrawer::~BulletDebugDrawer()
{
	// Cleanup VBO and shader
	if (_vertexBuffer)
	{
		delete _vertexBuffer;
	}
	if (_shader)
	{
		delete _shader;
	}
}

void BulletDebugDrawer::drawLine(const btVector3& from, const btVector3& to, const btVector3& color)
{
	// update vertices
	// TODO: build real box around the line
	_vertices.clear();
	_vertices.push_back(vec3(from.getX(), from.getY(), from.getZ()));
	_vertices.push_back(vec3(to.getX(), to.getY(), to.getZ()));
	_vertices.push_back(vec3(to.getX() - 0.1, to.getY() - 0.1, to.getZ() - 0.1));
	_vertices.push_back(vec3(from.getX(), from.getY(), from.getZ()));
	_vertices.push_back(vec3(to.getX(), to.getY(), to.getZ()));
	_vertices.push_back(vec3(to.getX() + 0.1, to.getY() + 0.1, to.getZ() + 0.1));

	_vertices.push_back(vec3(from.getX(), from.getY(), from.getZ()));
	_vertices.push_back(vec3(to.getX(), to.getY(), to.getZ()));
	_vertices.push_back(vec3(from.getX() - 0.1, from.getY() - 0.1, from.getZ() - 0.1));
	_vertices.push_back(vec3(from.getX(), from.getY(), from.getZ()));
	_vertices.push_back(vec3(to.getX(), to.getY(), to.getZ()));
	_vertices.push_back(vec3(from.getX() + 0.1, from.getY() + 0.1, from.getZ() + 0.1));

	_vertexBuffer->update(_vertices);

	_shader->activate();

	glm::mat4 ProjectionMatrix;
	glm::mat4 ViewMatrix;
	glm::mat4 MVP;
	CameraInfo* camInfo = CameraManager::getCameraInfo(0);
	if (camInfo)
	{
		// Compute the MVP matrix from keyboard and mouse input
		ProjectionMatrix = camInfo->_projectionMatrix;
		ViewMatrix = camInfo->_viewMatrix;
	}
	MVP = ProjectionMatrix * ViewMatrix * _modelMatrix;

	// Send our transformation to the currently bound shader, 
	// in the "MVP" uniform
	glUniformMatrix4fv(_matrixID, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(_modelMatrixID, 1, GL_FALSE, &_modelMatrix[0][0]);
	glUniformMatrix4fv(_viewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);

	_vertexBuffer->activate();

	// Draw the triangles !
	SheepEngineMain::addRenderedVertices(_vertices.size());
	glDrawArrays(GL_TRIANGLES, 0, _vertices.size());

	_vertexBuffer->deactivate();
}

void BulletDebugDrawer::draw3dText(const btVector3& location, const char* textString)
{
}

void BulletDebugDrawer::reportErrorWarning(const char* warningString)
{
	printf(warningString);
}

void BulletDebugDrawer::drawContactPoint(const btVector3& pointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color)
{
}