#pragma once

#include "LinearMath\btIDebugDraw.h"

#include "Resource.hpp"
#include "Texture.hpp"
#include "Shader.hpp"

#include <vector>

#include <GL/glew.h>

#include <glm/glm.hpp>

namespace SheepEngine
{
	class BulletDebugDrawer : public btIDebugDraw
	{
	public:
		BulletDebugDrawer();
		virtual ~BulletDebugDrawer();

		virtual void drawLine(const btVector3& from, const btVector3& to, const btVector3& color);

		virtual void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color);

		virtual void reportErrorWarning(const char* warningString);

		virtual void draw3dText(const btVector3& location, const char* textString);

		virtual void setDebugMode(int debugMode){ _debugMode = debugMode; }

		virtual int  getDebugMode() const { return _debugMode; }

	private:
		int _debugMode;

		glm::mat4 _modelMatrix;
		VertexArray _vertexArray;
		Shader* _shader;
		VertexBuffer* _vertexBuffer;
		GLuint _matrixID;
		GLuint _viewMatrixID;
		GLuint _modelMatrixID;

		std::vector<glm::vec3> _vertices;
	};
}

