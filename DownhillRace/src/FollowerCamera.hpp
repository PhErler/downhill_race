#pragma once

#include "Camera.hpp"
#include "PhysicsGameObject.hpp"

namespace SheepEngine
{
	class FollowerCamera : public Camera
	{
	public:
		FollowerCamera(PhysicsGameObject* followThis);

		virtual void update(double dt);

	private:
		PhysicsGameObject* _followThis;
	};
}

