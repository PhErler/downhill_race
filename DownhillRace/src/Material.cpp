#include "Material.hpp"

#include "SheepEngineMain.hpp"
#include "CameraManager.hpp"
#include "Texture.hpp"
#include "VboIndexer.hpp"

using namespace std;
using namespace glm;
using namespace SheepEngine;

Material::Material(
	std::string modelPath,
	std::string texturePath,
	std::string vertexShaderPath,
	std::string fragmentShaderPath)
{
	_shader = ResourceManager::getShader(vertexShaderPath, fragmentShaderPath);
	GLuint programID = _shader->getResourceID();

	// Get a handle for our "MVP" uniform
	_mvpMatrixID = glGetUniformLocation(programID, "MVP");
	_viewMatrixID = glGetUniformLocation(programID, "V");
	_modelMatrixID = glGetUniformLocation(programID, "M");

	// Load the texture using any two methods
	_texture = ResourceManager::getTexture(texturePath);

	// Get a handle for our DiffuseTextureSampler uniform
	_textureID = glGetUniformLocation(programID, "DiffuseTextureSampler");

	// Read our .obj file
	if (modelPath.length() != 0)
	{
		_modelData = ResourceManager::getModelData(modelPath);
	}

	// Get a handle for our "LightPosition" uniform
	glUseProgram(programID); 
	_invLightDirID = glGetUniformLocation(programID, "invLightDirection_worldspace");
	_lightColorID = glGetUniformLocation(programID, "LightColor");
	_ambientColorID = glGetUniformLocation(programID, "AmbientColor");
	_specularColorID = glGetUniformLocation(programID, "SpecularColor");
	_textureTilingID = glGetUniformLocation(programID, "TextureTiling");

	// shadow mapping stuff
	_lightBiasMVPID = glGetUniformLocation(programID, "LightBiasMVP");
	_shadowMapID = glGetUniformLocation(programID, "shadowMap");
	_shadowMappingSampleDistanceID = glGetUniformLocation(_shader->getResourceID(), "ShadowMappingSampleDistance");
}

bool Material::isVisible(const glm::mat4 &MVP)
{
	vector<vec3> aabbVertices;
	aabbVertices.reserve(8);
	aabbVertices.push_back(vec3(_modelData->_minBB.x, _modelData->_minBB.y, _modelData->_minBB.z));
	aabbVertices.push_back(vec3(_modelData->_minBB.x, _modelData->_minBB.y, _modelData->_maxBB.z));
	aabbVertices.push_back(vec3(_modelData->_minBB.x, _modelData->_maxBB.y, _modelData->_minBB.z));
	aabbVertices.push_back(vec3(_modelData->_minBB.x, _modelData->_maxBB.y, _modelData->_maxBB.z));
	aabbVertices.push_back(vec3(_modelData->_maxBB.x, _modelData->_minBB.y, _modelData->_minBB.z));
	aabbVertices.push_back(vec3(_modelData->_maxBB.x, _modelData->_minBB.y, _modelData->_maxBB.z));
	aabbVertices.push_back(vec3(_modelData->_maxBB.x, _modelData->_maxBB.y, _modelData->_minBB.z));
	aabbVertices.push_back(vec3(_modelData->_maxBB.x, _modelData->_maxBB.y, _modelData->_maxBB.z));

	for (vec3 &vertex : aabbVertices)
	{
		vec4 posNDC = MVP * vec4(vertex, 1);

		// do not homogenize, test with w to keep signs
		if (posNDC.x >= -posNDC.w && posNDC.x < posNDC.w ||
			posNDC.y >= -posNDC.w && posNDC.y < posNDC.w ||
			posNDC.z >= -posNDC.w && posNDC.z < posNDC.w)
		{
			return true;
		}
	}

	return false;
}

void Material::render(
	int playerNumber,
	const glm::mat4 &modelMatrix,
	const glm::mat4 &viewMatrix,
	const glm::mat4 &projectionMatrix)
{
	if (!_modelData || !_texture)
		return;

	// Compute the MVP matrix
	glm::mat4 MVP = projectionMatrix * viewMatrix * modelMatrix;

	if (SheepEngineMain::isCullingEnabled() && !isVisible(MVP))
	{
		return;
	}

	_shader->activate();

	glm::mat4 biasMatrix(
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0
	);

	glm::mat4 lightViewMatrix = SheepEngineMain::getLightViewMatrix();
	glm::mat4 lightProjectionMatrix = SheepEngineMain::getLightProjectionMatrix();
	glm::mat4 lightBiasMVP = biasMatrix * lightProjectionMatrix * lightViewMatrix * modelMatrix;

	// Send our transformation to the currently bound shader, 
	// in the "MVP" uniform
	glUniformMatrix4fv(_mvpMatrixID, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(_modelMatrixID, 1, GL_FALSE, &modelMatrix[0][0]);
	glUniformMatrix4fv(_viewMatrixID, 1, GL_FALSE, &viewMatrix[0][0]);
	glUniformMatrix4fv(_lightBiasMVPID, 1, GL_FALSE, &lightBiasMVP[0][0]);

	vec3 lightDir = SheepEngineMain::getLightDir();
	vec3 lightColor = SheepEngineMain::getLightColor();
	vec3 ambientColor = SheepEngineMain::getAmbientColor();
	vec3 specularColor = SheepEngineMain::getSpecularColor();
	glUniform3f(_invLightDirID, -lightDir.x, -lightDir.y, -lightDir.z);
	glUniform3f(_lightColorID, lightColor.x, lightColor.y, lightColor.z);
	glUniform3f(_ambientColorID, ambientColor.x, ambientColor.y, ambientColor.z);
	glUniform3f(_specularColorID, specularColor.x, specularColor.y, specularColor.z);
	glUniform2f(_textureTilingID, _textureTiling.x, _textureTiling.y);
	glUniform1f(_shadowMappingSampleDistanceID, SheepEngineMain::getShadowMappingSampleDistance());

	_texture->activate(0);
	// Set our DiffuseTextureSampler to user Texture Unit 0
	glUniform1i(_textureID, 0);

	SheepEngineMain::getShadowTexture()->activate(1);
	glUniform1i(_shadowMapID, 1);

	_modelData->_vertexBuffer.activate();
	_modelData->_uvBuffer.activate();
	_modelData->_normalBuffer.activate();

	glDisable(GL_BLEND);

	// Draw the triangles!
	SheepEngineMain::addRenderedVertices(getModelData()->_vertexBuffer.getVertices().size());
	if (SheepEngineMain::wireFrameMode == true)
	{
		glDrawArrays(GL_LINES, 0, getModelData()->_vertexBuffer.getVertices().size());
	}
	else
	{
		glDrawArrays(GL_TRIANGLES, 0, getModelData()->_vertexBuffer.getVertices().size());
	}	

	_modelData->_vertexBuffer.deactivate();
	_modelData->_uvBuffer.deactivate();
	_modelData->_normalBuffer.deactivate();

	SheepEngineMain::getShadowTexture()->deactivate(1);
	_texture->deactivate(0);
	_shader->deactivate();
}


void Material::renderZ(
	const glm::mat4 &modelMatrix,
	const glm::mat4 &lightViewMatrix,
	const glm::mat4 &lightProjectionMatrix)
{
	SheepEngineMain::getShadowFrameBuffer()->activate();
	SheepEngineMain::getdepthProgramID()->activate();
	_modelData->_vertexBuffer.activate();

	mat4 mPlVl = lightProjectionMatrix * lightViewMatrix * modelMatrix;

	glUniformMatrix4fv(SheepEngineMain::getLightMVPID(), 1, GL_FALSE,
		&mPlVl[0][0]);

	SheepEngineMain::addRenderedVertices(getModelData()->_vertexBuffer.getVertices().size());
	glDrawArrays(GL_TRIANGLES, 0, getModelData()->_vertexBuffer.getVertices().size());

	_modelData->_vertexBuffer.deactivate();
	SheepEngineMain::getdepthProgramID()->deactivate();
	SheepEngineMain::getShadowFrameBuffer()->deactivate();
}

Material::~Material()
{
	_shader.reset();
	_modelData.reset();
	_texture.reset();
}

btTriangleMesh* Material::getTriangleMesh()
{
	// Triangle Collision Shape
	btTriangleMesh *mTriMesh = new btTriangleMesh();

	std::vector<glm::vec3> vertices = _modelData->_vertexBuffer.getVertices();
	for (unsigned int i = 0; i < vertices.size(); i += 3)
	{

		btVector3 v0(vertices[i].x, vertices[i].y, vertices[i].z);
		btVector3 v1(vertices[i + 1].x, vertices[i + 1].y, vertices[i + 1].z);
		btVector3 v2(vertices[i + 2].x, vertices[i + 2].y, vertices[i + 2].z);

		// test output
		/*if (i == 0){
		cout << "Collision" << endl;
		cout << v0.x() << ", " << v0.y() << ", " << v0.z() << endl;
		cout << v1.x() << ", " << v1.y() << ", " << v1.z() << endl;
		cout << v2.x() << ", " << v2.y() << ", " << v2.z() << endl;
		*/

		mTriMesh->addTriangle(v0, v1, v2, true);
	}

	return mTriMesh;
}

void Material::switchSamplingMethod(int newMinSamplingMethod, int newMaxSamplingMethod)
{
	if (_texture)
	{
		_texture->switchSamplingMethod(newMinSamplingMethod, newMaxSamplingMethod);
	}
}