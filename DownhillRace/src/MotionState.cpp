#include "MotionState.hpp"

#include "PhysicsGameObject.hpp"

#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace std;
using namespace glm;
using namespace SheepEngine;

MotionState::MotionState(PhysicsGameObject *gameObject) 
: _gameObject(gameObject)
{
}

void MotionState::setWorldTransform(const btTransform &worldTrans) {
	if (_gameObject)
	{
		mat4 tempMat;
		worldTrans.getOpenGLMatrix(glm::value_ptr(tempMat));

		tempMat = glm::scale(tempMat, _gameObject->getScale());

		_gameObject->setModelMatrix(tempMat);
	}
}

void MotionState::getWorldTransform(btTransform& worldTrans) const
{
	if (_gameObject)
	{
		mat4 tempMat = _gameObject->getModelMatrix();
		worldTrans.setFromOpenGLMatrix(glm::value_ptr(tempMat));
	}
}