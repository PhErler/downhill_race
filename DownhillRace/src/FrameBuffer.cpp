#include "FrameBuffer.hpp"

#include "RenderTexture.hpp"
#include "SheepEngineMain.hpp"

using namespace std;
using namespace SheepEngine;

FrameBuffer::FrameBuffer(RenderTexture* colorTexture, RenderTexture* depthTexture)
{
	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	glGenFramebuffers(1, &_resourceID);
	glBindFramebuffer(GL_FRAMEBUFFER, _resourceID);

	updateRenderTexture(colorTexture, depthTexture);

	clear(colorTexture != nullptr, depthTexture != nullptr);
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

FrameBuffer::~FrameBuffer()
{
	glDeleteFramebuffers(1, &_resourceID);
}

void FrameBuffer::updateRenderTexture(RenderTexture* colorTexture, RenderTexture* depthTexture)
{
	glBindFramebuffer(GL_FRAMEBUFFER, _resourceID);

	if (depthTexture){
		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture->getResourceID(), 0);

		_width = depthTexture->getWidth();
		_height = depthTexture->getHeight();

		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		assert(status == GL_FRAMEBUFFER_COMPLETE);
	}

	if (colorTexture){
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, colorTexture->getResourceID(), 0);

		// Set the list of draw buffers.
		GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers

		_width = colorTexture->getWidth();
		_height = colorTexture->getHeight();

		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		assert(status == GL_FRAMEBUFFER_COMPLETE);
	}
	else{
		glDrawBuffer(GL_NONE);	// No color output in the bound framebuffer, only depth.
	}

	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FrameBuffer::activate()
{
	glBindFramebuffer(GL_FRAMEBUFFER, _resourceID);
	SheepEngineMain::setViewPort(Rect(0, 0, _width, _height));
}

void FrameBuffer::deactivate()
{
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//glViewport(0, 0, SheepEngineMain::getWindowParams()._xRes, SheepEngineMain::getWindowParams()._yRes);
}

void FrameBuffer::clear(bool clearColor, bool clearDepth, 
	float r, float g, float b, float a)
{
	activate();
	
	glClearColor(r, g, b, a);
	if (clearColor)
	{
		glClear(GL_COLOR_BUFFER_BIT);
	}
	
	if (clearDepth)
	{
		glClear(GL_DEPTH_BUFFER_BIT);
	}
	
	deactivate();
}